package vue;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;

import javafx.application.Platform;
import javafx.fxml.FXML;

public class Controller {
    @FXML
    private Label secondes;

    @FXML
    public Label centiemes;

    private modele.Chronometre chrono;

    public void startButtonClicked(ActionEvent actionEvent) {
        secondes.setText("start");
    }

    public void stopButtonClicked(ActionEvent actionEvent) {
        chrono.stop();
    }

    public void update() {
        Platform.runLater(new Runnable() {
            public void run() {
                int cent = chrono.getCentiemes();
                centiemes.setText (cent < 10 ? "0" + cent : "" + cent);
                int sec = chrono.getSecondes();
                secondes.setText(sec < 10 ? "0" + sec : "" + sec);
            }
        });
    }


    public void setChrono(modele.Chronometre chrono){
        this.chrono = chrono;
    }


}
