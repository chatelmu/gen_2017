package vue;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "vue.fxml"
                )
        );
        Parent root = loader.load();
        controller = (Controller)loader.getController();

        primaryStage.setTitle("Chronomètre");
        primaryStage.setScene(new Scene(root, 200, 100));
        primaryStage.show();

        modele.Chronometre chrono = new modele.Chronometre(controller);
        // —> Création d’un objet Chronometre, associé au contrôleur de l’interface graphique
        //    La variable interfaceController de l’objet Chronometre est maintenant une référence sur le contrôleur

        chrono.go();
        // —> Envoi du message go() au chronomètre
        //	Ce dernier affiche Chronometre: Reception du message Go
        //	Puis envoie le message update() au contrôleur de l’interface

        chrono.stop();
        // —> Envoi du message stop() au chronomètre, comportement  identique à go()

        controller.setChrono(chrono);   // Pour communiquer au contrôleur la référence sur l'objet chronometre


    }


    public static void main(String[] args) {
        launch(args);
    }
}
