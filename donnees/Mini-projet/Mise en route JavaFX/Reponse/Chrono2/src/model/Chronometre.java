package model;

/**
 * Created by Eric on 26.09.16.
 */
//------------------------------------------------------------
    public class Chronometre implements Runnable {


        private sample.Controller interfaceController;	// Association avec l'interface utilisateur
        private Thread activite;			// Activité interne
        private int cpt100;					// Compteur centiemes
        private int sec;					// Compteur des secondes

        enum Etat {ACTIF, EN_PAUSE, ARRET};
        private Etat etat=Etat.EN_PAUSE;

        // Constructeur
        public Chronometre (sample.Controller  ic) {
            interfaceController = ic;
            activite = new Thread(this);
            activite.start();
        }

        public void run() {
            while (true) {
                try {Thread.sleep(10);}
                catch (InterruptedException e) {}
                if (etat == Etat.ACTIF) incrementer();
            }
        }


        // Interrogateurs
        public int getSecondes() {
            return sec;
        }
        public int getCentiemes() {
            return cpt100;
        }

        // Messages envoyés par le contrôleur de l'interface graphique
        public void go() {
            etat=Etat.ACTIF;
        }

        public void stop() {
            if (etat==Etat.EN_PAUSE) {
                etat=Etat.ARRET;
                cpt100=0;
                sec=0;
                this.interfaceController.update();	// Signaler mise à jour à l'interface graphique
            }
            else if (etat==Etat.ACTIF){
                etat=Etat.EN_PAUSE;
            }
        }

        // Modificateurs
        private void incrementer() {
            // Incrementation du chrono: un centieme s'est ecoulé
            cpt100 = ++cpt100%100;
            if (cpt100==0) sec++;
            this.interfaceController.update();	// Signaler mise à jour à l'interface graphique
        }

    }