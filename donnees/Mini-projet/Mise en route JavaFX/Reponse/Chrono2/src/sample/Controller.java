package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;

import javafx.application.Platform;

public class Controller {
    public Label secondes;
    public Label centiemes;
    private model.Chronometre chrono;

    public void startButtonClicked(ActionEvent actionEvent) {
        chrono.go();
    }

    public void stopButtonClicked(ActionEvent actionEvent) {
        chrono.stop();
    }

    public void update() {
        Platform.runLater(new Runnable() {
            public void run() {
                int cent = chrono.getCentiemes();
                centiemes.setText (cent < 10 ? "0" + cent : "" + cent);
                int sec = chrono.getSecondes();
                secondes.setText(sec < 10 ? "0" + sec : "" + sec);
            }
        });
    }

    public void setChrono(model.Chronometre chrono){
        this.chrono = chrono;
    }

}
