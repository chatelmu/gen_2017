package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public sample.Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(
                        "sample.fxml"
                )
        );
        Parent root = loader.load();
        controller = (sample.Controller)loader.getController();

        primaryStage.setTitle("Chronometre");
        primaryStage.setScene(new Scene(root, 200, 100));
        primaryStage.show();

        model.Chronometre chrono = new model.Chronometre(controller);
        chrono.go();
        chrono.stop();
        controller.setChrono(chrono);   // Pour communiquer au contrôleur la référence sur l'objet chronometre
    }


    public static void main(String[] args) {
        launch(args);
    }
}
