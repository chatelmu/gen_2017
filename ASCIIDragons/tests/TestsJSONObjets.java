import Objets.*;
import Utilitaires.GsonListeObjetsAdapter;
import Utilitaires.GsonObjetAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Classe de tests JUnit permettant d'observer le bon fonctionnement de la (dé)sérialisation polymorphique (classes
 * GsonObjetAdapter et GsonListeObjetsAdapter.
 */
public class TestsJSONObjets
{
   private Gson gson;
   private static Objet objetTest;
   private static ListeObjets listeObjetsTest;
   private static String objetTestJSON;

   /**
    * Méthode appellée avant de run la classe de test, permet d'initialiser les objets qui serviront aux tests.
    */
   @BeforeClass
   public static void avantClasseTests()
   {
      objetTest = new Arme("JUnit-Sword", 10, "Arme");
      listeObjetsTest = new ListeObjets();
      listeObjetsTest.ajouterObjet(new Armure("Armure en peau de test", 0, 0, 40, 0, "Armure"));
      listeObjetsTest.ajouterObjet(new Arme("Sceptre de test", 20, "Arme"));
      listeObjetsTest.ajouterObjet(new Pomme("Pomme de test", 10, "Pomme"));
      listeObjetsTest.ajouterObjet(new Joyau("Joyau de réussite", 100, "Joyau"));

      objetTestJSON = "{\"attaque\":10,\"id\":0,\"nom\":\"JUnit-Sword\",\"type\":\"Arme\"}";
   }

   /**
    * Méthode appellée avant l'exécution des tests, permet d'initialiser les instances sur lesquels vont s'effectuer
    * les tests.
    */
   @Before
   public void avantTests()
   {
      GsonBuilder gsonBuilder = new GsonBuilder();
      gsonBuilder.registerTypeAdapter(ListeObjets.class, new GsonListeObjetsAdapter());
      gsonBuilder.registerTypeAdapter(Objet.class, new GsonObjetAdapter());
      gson = gsonBuilder.create();
   }

   /**
    * (Test pour les tests) On regarde si le toString affiche bien ce qu'on veut.
    */
   @Test
   public void toStringFonctionnel()
   {
      assertTrue(objetTest.toString().equals("JUnit-Sword"));
   }

   /**
    * Test pour voir si la sérialisation d'un objet retourne bien une String et que cette string est non-nulle.
    */
   @Test
   public void serialisationObjetRetourneBienUneString()
   {
      String testString = gson.toJson(objetTest);

      assertNotNull(testString);
      assertTrue(testString.getClass() == String.class);
   }

   /**
    * Test pour voir si la Jsonification est correct (la string de référence est intialisée en dessus).
    */
   @Test
   public void serialisationObjetEstCorrecte()
   {
      String testString = gson.toJson(objetTest);
      System.out.println(testString);
      assertTrue(testString.equals(objetTestJSON));
   }

   /**
    * Test pour voir si la désérialisation d'une string JSON retourne bien un objet et que celui-ci est non-nul.
    */
   @Test
   public void deserialisationObjetRetourneBienUnObjet()
   {
      Objet testObjet = gson.fromJson(objetTestJSON, Objet.class);

      assertNotNull(testObjet);
      assertTrue(testObjet.getClass() == Arme.class);
   }

   /**
    * Test pour voir si les champs obtenus après la désérialisation correspondent bien à ceux présents dans l'objet
    * original.
    */
   @Test
   public void deserialisationObjetEstCorrecte()
   {
      Objet testObjet = gson.fromJson(objetTestJSON, Objet.class);

      assertTrue(testObjet.getNom().equals(objetTest.getNom())
                       && testObjet.getType().equals(objetTest.getType()));
   }

   /**
    * Test tout en un pour les listeObjets.
    */
   @Test
   public void serialisationDeserialisationListeObjets()
   {
      String testString = gson.toJson(listeObjetsTest);

      // Afin de voir l'objet Jsonifié
      System.out.println(testString);

      // Vérification que sérialisation donne une string non-nulle
      assertNotNull(testString);
      assertTrue(testString.getClass() == String.class);

      ListeObjets listeRecuperee = gson.fromJson(testString, ListeObjets.class);

      // Vérification que la désérialisation donne une listeObjet non-nulle
      assertNotNull(listeRecuperee);
      assertTrue(listeRecuperee.getClass() == ListeObjets.class);

      // Vérification que les deux liste sont bien les mêmes
      assertTrue(listeRecuperee.getListe().get(0).getNom().equals(listeObjetsTest.getListe().get(0).getNom())
                       && listeRecuperee.getListe().get(0).getType().equals(listeObjetsTest.getListe().get(0).getType())
                       && listeRecuperee.getListe().get(1).getNom().equals(listeObjetsTest.getListe().get(1).getNom())
                       && listeRecuperee.getListe().get(1).getType().equals(listeObjetsTest.getListe().get(1).getType())
                       && listeRecuperee.getListe().get(2).getNom().equals(listeObjetsTest.getListe().get(2).getNom())
                       && listeRecuperee.getListe().get(2).getType().equals(listeObjetsTest.getListe().get(2).getType())
                       && listeRecuperee.getListe().get(3).getNom().equals(listeObjetsTest.getListe().get(3).getNom())
                       && listeRecuperee.getListe().get(3).getType().equals(listeObjetsTest.getListe().get(3).getType()));
   }
}
