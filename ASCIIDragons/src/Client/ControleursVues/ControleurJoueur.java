package Client.ControleursVues;

import Objets.*;
import Protocole.*;
import Protocole.ReponsePossibiliteDeplacement;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe représentant le controleur de la fenêtre du joueur.
 */
public class ControleurJoueur
{
   @FXML
   private TextArea logs;

   @FXML
   private TextArea asciiDecor, asciiMonstre, asciiCoffre, asciiJoueur;

   @FXML
   private ProgressIndicator resPlante, resFeu, resEau, resAir;

   @FXML
   private ProgressBar vie;

   @FXML
   private Label attaque, labelVie;

   @FXML
   private Button attaquer, fuir, prendre, laisser;

   @FXML
   private Button nord, sud, est, ouest;

   // Membres non-FXML
   private Controleur controleur;
   private String coffre;
   private Case caseCourante;
   private boolean nordAccessible;
   private boolean sudAccessible;
   private boolean estAccessible;
   private boolean ouestAccessible;
   private int nbTours = 0;

   /**
    * Méthode appellée quand le bouton de direction nord est cliqué.
    */
   @FXML
   public void dirNordPressed()
   {
      // Récupération de la nouvelle case et mise à jour
      ReponsePossibiliteDeplacement reponse = controleur.seDeplacer(Integer.toString(0));

      mettreAjourCase(reponse);

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Quand c'est notre tour, on remet correctement tous les boutons et c'est parti pour le tour suivant
      attendreTour.setOnSucceeded(event -> mettreAjourCase(reponse));

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton de direction sud est cliqué.
    */
   @FXML
   public void dirSudPressed()
   {
      // Récupération de la nouvelle case et mise à jour
      ReponsePossibiliteDeplacement reponse = controleur.seDeplacer(Integer.toString(1));

      mettreAjourCase(reponse);

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Quand c'est notre tour, on remet correctement tous les boutons et c'est parti pour le tour suivant
      attendreTour.setOnSucceeded(event -> mettreAjourCase(reponse));

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton de direction est est cliqué.
    */
   @FXML
   public void dirEstPressed()
   {
      // Récupération de la nouvelle case et mise à jour
      ReponsePossibiliteDeplacement reponse = controleur.seDeplacer(Integer.toString(2));

      mettreAjourCase(reponse);

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Quand c'est notre tour, on remet correctement tous les boutons et c'est parti pour le tour suivant
      attendreTour.setOnSucceeded(event -> mettreAjourCase(reponse));

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton de direction ouest est cliqué.
    */
   @FXML
   public void dirOuestPressed()
   {
      // Récupération de la nouvelle case et mise à jour
      ReponsePossibiliteDeplacement reponse = controleur.seDeplacer(Integer.toString(3));

      mettreAjourCase(reponse);

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Quand c'est notre tour, on remet correctement tous les boutons et c'est parti pour le tour suivant
      attendreTour.setOnSucceeded(event -> mettreAjourCase(reponse));

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton d'attaque est cliqué.
    */
   @FXML
   public void attaquerPressed()
   {
      // Récupération des nouvelles données du joueur après l'attaque
      ReponseEtatFinDeTour reponse = controleur.attaquer();

      boolean monstreMort = false;

      mettreAjourEtatJoueur(reponse);

      // Si le joueur est mort
      if (reponse.getEtatJoueur().getPvActuels() <= 0)
      {
         joueurMort();
      }

      // Si le monstre est mort
      if (reponse.getPvMonstre() <= 0)
      {
         monstreMort = true;

         // L'objet disponible est afficher
         if (caseCourante.getObjet() != null)
         {
            ajouterLog("Le monstre est mort, il a laché un " + caseCourante.getObjet().getNom());
         }

         asciiMonstre.setText("");
         asciiCoffre.setText("");
      }

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Quand c'est de nouveau notre tour, on remet tout bien les boutons et c'est reparti
      boolean finalMonstreMort = monstreMort;
      attendreTour.setOnSucceeded(new EventHandler<WorkerStateEvent>()
      {
         @Override
         public void handle(WorkerStateEvent event)
         {
            if (finalMonstreMort)
            {
               prendre.setDisable(false);
               laisser.setDisable(false);
            }
            else
            {
               attaquer.setDisable(false);
               fuir.setDisable(false);
            }
         }
      });

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton fuir est cliqué.
    */
   @FXML
   public void fuirPressed()
   {
      // On demande au serveur si la fuite est possible
      boolean fuitePossible = controleur.fuitePossible();

      // Si la fuite est possible, on récupère la nouvelle case
      if (fuitePossible)
      {
         ajouterLog("Vous avez réussi à fuire, bravo");

         ReponsePossibiliteDeplacement nouvelleCase = controleur.getNouvelleCase();
         mettreAjourCase(nouvelleCase);

         // Attente du prochain tour
         Task<Void> attendreTour = new Task<Void>()
         {
            @Override
            protected Void call() throws Exception
            {
               attendreMonTour();
               return null;
            }
         };

         // Quand c'est de nouveau notre tour, on remet les boutons qu'il faut actifs
         attendreTour.setOnSucceeded(event -> mettreAjourCase(nouvelleCase));

         new Thread(attendreTour).start();
      }
      // La fuite est impossible, on récupère le nouvel état du joueur et du monstre
      else
      {
         // Nos nouveaux points de vie après la vengeance du monstre
         double nouveauxPv = controleur.getVengeanceMonstre();

         ajouterLog("Le monstre vous retient et il n'est pas très gentil. Il vous reste " + nouveauxPv +
                          " points de vie");

         // Si on est mort de la réplique du monstre
         if (nouveauxPv <= 0)
         {
            joueurMort();
         }
         // Sinon mise à jour de l'affichage
         else
         {
            double pourcentVie = nouveauxPv / Initialisations.PV_JOUEUR_MAX;
            vie.setProgress(pourcentVie);
         }

         // Attente du prochain tour
         Task<Void> attendreTour = new Task<Void>()
         {
            @Override
            protected Void call() throws Exception
            {
               attendreMonTour();
               return null;
            }
         };

         // Quand c'est de nouveau notre tour, on met les bons boutons de nouveaux actifs
         attendreTour.setOnSucceeded(new EventHandler<WorkerStateEvent>()
         {
            @Override
            public void handle(WorkerStateEvent event)
            {
               attaquer.setDisable(false);
               fuir.setDisable(false);
            }
         });

         new Thread(attendreTour).start();
      }
   }

   /**
    * Méthode appellée quand le bouton prendre est cliqué
    */
   @FXML
   public void prendrePressed()
   {
      // On récupère les nouvelles caractéristiques du joueurs
      ReponseEtatFinDeTour nouvelEtat = controleur.prendreObjet();

      mettreAjourEtatJoueur(nouvelEtat);
      asciiCoffre.setText("");

      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Lorsque c'est de nouveau notre tour, on remet les bons boutons actifs
      attendreTour.setOnSucceeded(new EventHandler<WorkerStateEvent>()
      {
         @Override
         public void handle(WorkerStateEvent event)
         {
            nord.setDisable(!nordAccessible);
            sud.setDisable(!sudAccessible);
            est.setDisable(!estAccessible);
            ouest.setDisable(!ouestAccessible);
         }
      });

      new Thread(attendreTour).start();
   }

   /**
    * Méthode appellée quand le bouton laisser est cliqué.
    */
   @FXML
   public void laisserPressed()
   {
      // Attente du prochain tour
      Task<Void> attendreTour = new Task<Void>()
      {
         @Override
         protected Void call() throws Exception
         {
            attendreMonTour();
            return null;
         }
      };

      // Lorsque c'est de nouveau notre tour, on remet les bons boutons actifs
      attendreTour.setOnSucceeded(new EventHandler<WorkerStateEvent>()
      {
         @Override
         public void handle(WorkerStateEvent event)
         {
            nord.setDisable(!nordAccessible);
            sud.setDisable(!sudAccessible);
            est.setDisable(!estAccessible);
            ouest.setDisable(!ouestAccessible);
         }
      });

      new Thread(attendreTour).start();
   }

   /**
    * Méthode permettant de mettre à jour l'affichage en fonction d'un nouvel état donné.
    *
    * @param reponse Le nouvel état du joueur à afficher
    */
   private void mettreAjourEtatJoueur(ReponseEtatFinDeTour reponse)
   {
      // Quand les pv sont à -1000, c'est que les points de vies du monstres n'ont pas changés
      if (reponse.getPvMonstre() != -1000)
      {
         ajouterLog("Il vous reste " + reponse.getEtatJoueur().getPvActuels() + " points de vie et le monstre en a " +
                          "encore " + reponse.getPvMonstre());
      }

      // Affichage des informations reçues
      attaque.setText(String.valueOf(reponse.getEtatJoueur().getAttaque()));
      labelVie.setText(String.valueOf((int)reponse.getEtatJoueur().getPvActuels()));

      double pourcentVie = reponse.getEtatJoueur().getPvActuels() / reponse.getEtatJoueur().getPvMax();
      double pourcentProtAir = reponse.getEtatJoueur().getDefenseAir() / 100.0;
      double pourcentProtEau = reponse.getEtatJoueur().getDefenseEau() / 100.0;
      double pourcentProtFeu = reponse.getEtatJoueur().getDefenseFeu() / 100.0;
      double pourcentProtTerre = reponse.getEtatJoueur().getDefenseTerre() / 100.0;

      vie.setProgress(pourcentVie);
      resAir.setProgress(pourcentProtAir);
      resEau.setProgress(pourcentProtEau);
      resFeu.setProgress(pourcentProtFeu);
      resPlante.setProgress(pourcentProtTerre);
   }

   /**
    * Méthode permettant de mettre à jour l'affichage en fonction de la nouvelle case reçue.
    *
    * @param deplacement Un objet contenant la nouvelle case, et les nouveaux booléens d'accès
    */
   private void mettreAjourCase(ReponsePossibiliteDeplacement deplacement)
   {
      // Mise à jour du décor
      asciiDecor.setText(deplacement.getCase().getDecor().getImage());

      // Mise à jour des boutons de direction
      nord.setDisable(!deplacement.getNord());
      sud.setDisable(!deplacement.getSud());
      est.setDisable(!deplacement.getEst());
      ouest.setDisable(!deplacement.getOuest());

      // Mise à jour des boutons et ascii-arts en fonction de si il y a un monstre/objet ou pas
      // Si il y a ni monstre, ni objet
      if (deplacement.getCase().getMonstre() == null && deplacement.getCase().getObjet() == null)
      {
         asciiCoffre.setText("");
         asciiMonstre.setText("");

         attaquer.setDisable(true);
         fuir.setDisable(true);
         laisser.setDisable(true);
         prendre.setDisable(true);
      }
      // Si il y a un objet mais pas de monstre
      else if (deplacement.getCase().getMonstre() == null && deplacement.getCase().getObjet() != null)
      {
         asciiCoffre.setText(coffre);
         asciiMonstre.setText("");

         attaquer.setDisable(true);
         fuir.setDisable(true);
         prendre.setDisable(false);
         laisser.setDisable(false);
         ajouterLog("Il y a un(e) " + deplacement.getCase().getObjet().getNom() + ". Voulez-vous le ramassez ?");

         nord.setDisable(true);
         sud.setDisable(true);
         est.setDisable(true);
         ouest.setDisable(true);
      }
      // Si il y a un monstre mais pas d'objet
      else if (deplacement.getCase().getMonstre() != null && deplacement.getCase().getObjet() == null)
      {
         asciiMonstre.setText(deplacement.getCase().getMonstre().getImage());
         asciiCoffre.setText("");

         attaquer.setDisable(false);
         fuir.setDisable(false);
         prendre.setDisable(true);
         laisser.setDisable(true);

         nord.setDisable(true);
         sud.setDisable(true);
         est.setDisable(true);
         ouest.setDisable(true);
      }
      // Si il y a un monstre et un objet
      else if (deplacement.getCase().getMonstre() != null && deplacement.getCase().getObjet() != null)
      {
         asciiMonstre.setText(deplacement.getCase().getMonstre().getImage());
         asciiCoffre.setText(coffre);

         attaquer.setDisable(false);
         fuir.setDisable(false);
         prendre.setDisable(true);
         laisser.setDisable(true);

         nord.setDisable(true);
         sud.setDisable(true);
         est.setDisable(true);
         ouest.setDisable(true);
      }

      // Sauvegarde des possibilités de déplacement
      caseCourante = deplacement.getCase();
      sudAccessible = deplacement.getSud();
      nordAccessible = deplacement.getNord();
      estAccessible = deplacement.getEst();
      ouestAccessible = deplacement.getOuest();
   }

   /**
    * Méthode appellée au changement de fenêtre, celle-ci permet de passer le controleur général et d'initialiser la
    * nouvelle fenêtre.
    *
    * @param controleur Le controleur général envoyé par la fenêtre précédente
    */
   public void setDonnees(final Controleur controleur)
   {
      this.controleur = controleur;
      coffre = asciiCoffre.getText();

      // Work-arourd pour déterminer si on doit continuer après cet appel. Le joueur dont c'est le tour reçoit 11 alors
      // que les autres reçoivent 12
      int isItMyTurn = attendreMonTour();

      // Quelle que soit si c'est leur tour ou pas, les joueurs récupère tous leur case de départ et la set
      ReponsePossibiliteDeplacement reponse = controleur.recupereCaseOriginale();
      mettreAjourCase(reponse);

      // Initialisation de l'état du joueur
      attaque.setText(String.valueOf(Initialisations.ATTAQUE_JOUEUR_INIT));
      labelVie.setText(String.valueOf((int) Initialisations.PV_JOUEUR_INIT));

      double pourcentVie = Initialisations.PV_JOUEUR_INIT / Initialisations.PV_JOUEUR_MAX;
      double pourcentProtAir = Initialisations.DEFENSE_AIR_INIT / 100.0;
      double pourcentProtEau = Initialisations.DEFENSE_EAU_INIT / 100.0;
      double pourcentProtFeu = Initialisations.DEFENSE_FEU_INIT / 100.0;
      double pourcentProtTerre = Initialisations.DEFENSE_TERRE_INIT / 100.0;

      vie.setProgress(pourcentVie);
      resAir.setProgress(pourcentProtAir);
      resEau.setProgress(pourcentProtEau);
      resFeu.setProgress(pourcentProtFeu);
      resPlante.setProgress(pourcentProtTerre);

      // Tous ceux dont ce n'est pas le tour ce mettent en attente, l'autre passe tout droit
      if (isItMyTurn != ProtocoleASCIIDragons.CMD_TOUR_JOUEUR)
      {
         // Attente du prochain tour
         Task<Void> attendreTour = new Task<Void>()
         {
            @Override
            protected Void call() throws Exception
            {
               attendreMonTour();
               return null;
            }
         };

         // Lorsque c'est le tour de ce joueur, on remet la case avec les boutons correctement
         attendreTour.setOnSucceeded(new EventHandler<WorkerStateEvent>()
         {
            @Override
            public void handle(WorkerStateEvent event)
            {
               System.out.println("c'est mon tour");
               mettreAjourCase(reponse);
            }
         });

         new Thread(attendreTour).start();
      }
   }

   /**
    * Fonction privée permettant de mettre à jour la fenêtre à la mort du joueur
    */
   private void joueurMort()
   {
      ajouterLog("Vous êtes mort, attendez la fin de la partie pour connaitre votre score");
      vie.setProgress(0);
      attaquer.setDisable(true);
      prendre.setDisable(true);
      laisser.setDisable(true);
      fuir.setDisable(true);
      nord.setDisable(true);
      sud.setDisable(true);
      est.setDisable(true);
      ouest.setDisable(true);

      asciiJoueur.setText("");
   }

   /**
    * Fonction permettant de créer une nouvelle ligne de log et de l'ajouter proprement dans la fenêtre de logs.
    *
    * @param log La ligne de log à ajouter
    */
   private void ajouterLog(String log)
   {
      DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
      Date date = new Date();

      logs.setText("[" + dateFormat.format(date) + "] : " + log + "\n" + logs.getText());
   }

   /**
    * Fonction bloquante appellée lors de l'attente du tour.
    *
    * @return L'état renvoyé par le serveur (voir controleur.java/attendreTour)
    */
   private int attendreMonTour()
   {
      // On disable tous les boutons lors de l'attente
      attaquer.setDisable(true);
      prendre.setDisable(true);
      laisser.setDisable(true);
      fuir.setDisable(true);
      nord.setDisable(true);
      sud.setDisable(true);
      est.setDisable(true);
      ouest.setDisable(true);

      // Fonction bloquante
      int reponse = controleur.attendreTour();

      // TODO pas encore entiérement fonctionnel, à revoir
      if (reponse == ProtocoleASCIIDragons.CMD_TOUR_JOUEUR)
      {
         nbTours++;
      }
      else if (reponse == ProtocoleASCIIDragons.CMD_FIN_PARTIE)
      {
         String scores = controleur.attendreScore();
         ajouterLog(scores);
         attaquer.getParent().setDisable(true);
      }

      return reponse;
   }
}
