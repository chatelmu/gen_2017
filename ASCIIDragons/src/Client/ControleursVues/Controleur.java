package Client.ControleursVues;

import Client.Socket.SocketClient;
import Objets.*;
import Protocole.*;
import Utilitaires.GsonListeObjetsAdapter;
import Utilitaires.GsonObjetAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Classe gérant les interactions avec le serveur, envoie des requêtes et reçoit les réponses
 */
public class Controleur
{
   private SocketClient socketClient;  // Le socket du client
   private Gson gson;                  // L'instance gson chargée de la (dé)sérialisation

   /**
    * Constructeur du controleur
    *
    * @param socketClient Le socketClient créé par le client au moment de la demande de connexion
    */
   public Controleur(SocketClient socketClient)
   {
      this.socketClient = socketClient;

      // Création du gson ainsi qu'ajout des registres pour la (dé)sérialisation des objets qui présentent de l'héritage
      GsonBuilder gsonBuilder = new GsonBuilder();
      gsonBuilder.registerTypeAdapter(ListeObjets.class, new GsonListeObjetsAdapter());
      gsonBuilder.registerTypeAdapter(Objet.class, new GsonObjetAdapter());
      this.gson = gsonBuilder.create();
   }

   /**
    * Méthode permettant d'envoyer le pseudonyme pour la connexion au serveur.
    *
    * @param pseudonyme Le pseudonyme saisi par le joueur
    * @return Admin, joueur ou full dépendant du rôle qui nous a été attribué ou non par le serveur, admin = 1er socket
    * connecté (administrateur), joueur pour un joueur, full si le serveur est plein
    */
   public String connexion(String pseudonyme)
   {
      // Envoyer la requete de connexion au serveur
      RequeteConnexion requeteConnexion = new RequeteConnexion(pseudonyme);

      socketClient.print(gson.toJson(requeteConnexion));

      // Attendre la réponse du serveur
      String reponse = socketClient.read();
      System.out.println(reponse);
      return reponse;
   }

   /**
    * Méthode permettant de récupérer la liste des monstres de la base de données.
    *
    * @return Un objet listeMonstre avec les différents monstres peuplant la base de données
    */
   public ListeMonstres getListeMonstres()
   {
      ListeMonstres reponseDEJ = gson.fromJson(socketClient.read(), ListeMonstres.class);

      return reponseDEJ;
   }

   /**
    * Méthode permettant de récupérer la liste des décors de la base de données.
    *
    * @return Un objet listeDecors avec les différents décors peuplant la base de données
    */
   public ListeDecors getListeDecors()
   {
      ListeDecors reponseDEJ = gson.fromJson(socketClient.read(), ListeDecors.class);

      return reponseDEJ;
   }

   /**
    * Méthode permettant de récupérer la liste des objets de la base de données.
    *
    * @return Un objet listeObjets avec les différents objets peuplant la base de données
    */
   public ListeObjets getListeObjets()
   {
      ListeObjets reponseDEJ = gson.fromJson(socketClient.read(), ListeObjets.class);

      return reponseDEJ;
   }

   /**
    * Méthode permettant d'envoyer une nouvelle case, configurée par l'administrateur, au serveur.
    *
    * @param ligne     La ligne sur laquelle se trouve la case
    * @param colonne   La colonne sur laquelle se trouve la case
    * @param caseCreee La nouvelle case
    */
   public void envoyerCase(Integer ligne, Integer colonne, Case caseCreee)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_CONFIGURER_CASE, ligne.toString(),
                                                  colonne.toString(), gson.toJson(caseCreee));
      socketClient.print(gson.toJson(requete));
   }

   /**
    * Méthode permettant de signaler au serveur que la création de la partie est terminée et que le jeu peut commencer.
    */
   public void fermerConnexionAdmin()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_FERMER_SOCKET);
      socketClient.print(gson.toJson(requete));
   }

   /**
    * Méthode bloquante qui permet de bloquer les joueurs en attendant que l'admin aie créé la partie.
    */
   public void attendreCreationPartie()
   {
      String ok = socketClient.read();
      System.out.println("fin création partie : " + ok);
   }

   /**
    * Méthode bloquante qui permet de bloquer les joueurs lorsqu'ils attendent leur tour.
    *
    * @return Le code envoyé par le serveur, 11 = "c'est ton tour", 12 = "ce n'est pas ton tour", 13 = "fin de partie"
    */
   public int attendreTour()
   {
      String ok = socketClient.read();

      return Integer.parseInt(ok);
   }

   /**
    * Méthode bloquant qui permet de bloquer les joueurs en attendant la réception des scores.
    *
    * @return La string représentant les scores
    */
   public String attendreScore()
   {
      String scores = socketClient.read();

      return scores;
   }

   /**
    * Méthode permettant de signaler au serveur que le joueur souhaite se déplacer.
    *
    * @param direction La direction vers laquelle il veut se rendre 0=nord, 1=sud, 2=est, 3=ouest
    * @return Un objet contenant la nouvelle case ainsi que les 4 booléens indiquant si les directions sont accessibles
    */
   public ReponsePossibiliteDeplacement seDeplacer(String direction)
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_DEMANDE_DEPLACEMENT, direction);
      socketClient.print(gson.toJson(requete));

      ReponsePossibiliteDeplacement reponse = gson.fromJson(socketClient.read(), ReponsePossibiliteDeplacement.class);

      return reponse;
   }

   /**
    * Méthode permettant de signaler au serveur que le joueur souhaite attaquer le monstre sur sa case.
    *
    * @return Un objet représentant le nouvel état du joueur ainsi que les nouveaux points de vie du monstre
    */
   public ReponseEtatFinDeTour attaquer()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_ATTAQUER);
      socketClient.print(gson.toJson(requete));

      ReponseEtatFinDeTour reponse = gson.fromJson(socketClient.read(), ReponseEtatFinDeTour.class);

      return reponse;
   }

   /**
    * Méthode permettant de demander au serveur si on arrive à fuire le monstre.
    *
    * @return True si la fuite est possible, false sinon
    */
   public boolean fuitePossible()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_FUIR);
      socketClient.print(gson.toJson(requete));

      boolean reponse = Boolean.parseBoolean(socketClient.read());

      return reponse;
   }

   /**
    * Si on a appris que la fuite est possible, cette méthode est appelée pour récupérer la case sur la case on a fuit.
    *
    * @return La nouvelle possibilité déplacement (case + booléens directions)
    */
   public ReponsePossibiliteDeplacement getNouvelleCase()
   {
      return gson.fromJson(socketClient.read(), ReponsePossibiliteDeplacement.class);
   }

   /**
    * Si on a appris que la fuite est impossible, cette méthode est appelée pour récupérer les dégats que le monstre
    * nous inflige en représailles.
    *
    * @return Le nombre de dégats infligés par le monstre
    */
   public double getVengeanceMonstre()
   {
      return Double.parseDouble(socketClient.read());
   }

   /**
    * Méthode permettant de signaler au serveur que le joueur souhaite prendre l'objet présent sur la case.
    *
    * @return Un objet représentant le nouvel état du joueur après qu'il aie pris l'objet
    */
   public ReponseEtatFinDeTour prendreObjet()
   {
      RequeteServeur requete = new RequeteServeur(ProtocoleASCIIDragons.CMD_RAMASSER_OBJET);
      socketClient.print(gson.toJson(requete));

      ReponseEtatFinDeTour nouvelEtat = gson.fromJson(socketClient.read(), ReponseEtatFinDeTour.class);

      return nouvelEtat;
   }

   /**
    * Méthode permettant au client de récupérer la case sur laquelle il apparait avant le début de la partie.
    *
    * @return Un objet qui contient la nouvelle case ainsi que les booléens de 4 directions
    */
   public ReponsePossibiliteDeplacement recupereCaseOriginale()
   {
      ReponsePossibiliteDeplacement reponse = gson.fromJson(socketClient.read(), ReponsePossibiliteDeplacement.class);
      return reponse;
   }
}
