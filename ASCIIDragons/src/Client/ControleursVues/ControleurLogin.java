package Client.ControleursVues;

import Client.Socket.SocketClient;
import Serveur.Utilitaires.Journalisation;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class ControleurLogin
{
   private Controleur controleur;

   @FXML
   private Button connexion;

   @FXML
   private TextField adresse;

   @FXML
   private TextField pseudo;

   @FXML
   private Label erreur;

   @FXML
   public void connexionPressed() throws IOException
   {
      String adr = adresse.getCharacters().toString();
      String nom = pseudo.getCharacters().toString();

      boolean isConnected = true;

      // Si l'adresse n'est pas donnée
      if (adr.equalsIgnoreCase(""))
      {
         erreur.setText("ERREUR : Entrez une adresse pour le serveur");
      }

      // Si le pseudonyme n'est pas donné
      else if (nom.equalsIgnoreCase(""))
      {
         erreur.setText("ERREUR : Entrez votre pseudonyme");
      }

      // Si tout est ok
      else
      {
         SocketClient sc = null;

         // On test si on a réussi a établir la connexion avec le serveur
         try
         {
            sc = new SocketClient(adresse.getCharacters().toString().trim(), 4242);
         }
         catch (Exception e)
         {
            Journalisation.getERR().warning("Adresse serveur incorrecte");
            erreur.setText("Connexion au serveur impossible, contrôler l'adresse !");
            isConnected = false;
         }

         if (isConnected)
         {
            controleur = new Controleur(sc);
            String reponse = controleur.connexion(nom);

            // Déterminer le type de client connecté
            // Pour l'admin on ferme la fenetre connexion et on affiche celle de création
            if (reponse.equalsIgnoreCase("admin"))
            {
               afficherFenetreAdmin();
            }
            // Pour le joueur on affiche la fenetre d'attente
            else if (reponse.equalsIgnoreCase("joueur"))
            {
               erreur.setTextFill(Color.web("#009900"));
               erreur.setText("Connexion au serveur établie. Création de la partie en cours");
               connexion.setDisable(true);
               adresse.setDisable(true);
               pseudo.setDisable(true);

               // Mise en attente pour la création de la partie
               Task<Void> attendreDebut = new Task<Void>()
               {
                  @Override
                  public Void call() throws IOException
                  {
                     controleur.attendreCreationPartie();
                     return null;
                  }
               };

               attendreDebut.setOnSucceeded(event -> afficherFenetreJoueur());

               new Thread(attendreDebut).start();
            }

            // Si il y a déjà trop de joueurs connectés à cette partie
            else if (reponse.equalsIgnoreCase("full"))
            {
               erreur.setText("Désolé trop de joueurs sont déjà en attente pour cette partie");
            }
         }
      }
   }

   /**
    * Fonction privée permettant de changer de scène vers la fenêtre admin
    */
   private void afficherFenetreAdmin()
   {
      try
      {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Client/Vues/VueAdmin.fxml"));
         Parent root1 = fxmlLoader.load();
         Stage stage = (Stage) erreur.getScene().getWindow();
         stage.setTitle("Création de la partie");
         stage.getIcons().add(new Image("/Images/logoPage.png"));
         stage.setScene(new Scene(root1));
         stage.show();
         ControleurAdmin controleurAdmin = (ControleurAdmin) fxmlLoader.getController();
         controleurAdmin.setDonnees(controleur);
      }
      catch (Exception e)
      {
         Journalisation.getERR().severe("Erreur lors du changement de fenetre d'administration\n" + e.getMessage() + "\n");
         e.printStackTrace();
         erreur.setText("Impossible de charger la nouvelle fenetre...");
      }
   }

   /**
    * Fonction privée permettant de changer de scène vers la fenêtre joueur
    */
   private void afficherFenetreJoueur()
   {
      try
      {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Client/Vues/VueUser.fxml"));
         Parent root1 = fxmlLoader.load();
         Stage stage = (Stage) erreur.getScene().getWindow();
         stage.setTitle("ASCII Dragons");
         stage.setScene(new Scene(root1));
         stage.show();
         ControleurJoueur controleurJoueur = (ControleurJoueur) fxmlLoader.getController();
         controleurJoueur.setDonnees(controleur);
      }
      catch (Exception e)
      {
         Journalisation.getERR().severe("Erreur lors du changement de fenetre du joueur\n" + e.getMessage() + "\n");
         e.printStackTrace();
         erreur.setText("Impossible de charger la nouvelle fenetre...");
      }
   }
}
