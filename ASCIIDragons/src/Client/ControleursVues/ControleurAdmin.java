package Client.ControleursVues;

import Objets.*;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe représentant le controleur de la fenêtre d'administration.
 */
public class ControleurAdmin
{
   private Controleur controleur;            // Le controleur général
   private ArrayList<Monstre> listeMonstres; // La liste de tous les monstres existants dans la base de données
   private ArrayList<Decor> listeDecors;     // La liste de tous les décors existants dans la base de données
   private ArrayList<Objet> listeObjets;     // La liste de tous les objets existants dans la base de données

   @FXML
   private Label label;

   @FXML
   private ComboBox<Monstre> monstres;

   @FXML
   private ComboBox<Objet> objets;

   @FXML
   private ComboBox<Decor> decors;

   @FXML
   private ChoiceBox<Integer> ligneCarte;

   @FXML
   private ChoiceBox<Integer> colonneCarte;

   /**
    * Méthode appellée lors du clique sur le bouton "valider la case".
    */
   @FXML
   public void validerCasePressed()
   {
      // Récupération des champs saisis
      Integer ligneSelectionnee;
      Integer colonneSelectionnee;
      Monstre monstreSelectionne;
      Decor decorSelectionne;
      Objet objetSelectionne;

      ligneSelectionnee = ligneCarte.getSelectionModel().getSelectedItem();
      colonneSelectionnee = colonneCarte.getSelectionModel().getSelectedItem();
      monstreSelectionne = monstres.getSelectionModel().getSelectedItem();

      if(monstreSelectionne.getType() == null)
      {
         monstreSelectionne = null;
      }
      decorSelectionne = decors.getSelectionModel().getSelectedItem();
      objetSelectionne = objets.getSelectionModel().getSelectedItem();

      if(objetSelectionne.getType() == null)
      {
         objetSelectionne = null;
      }

      // Création et envoi de la nouvelle case
      Case caseCreee = new Case(decorSelectionne, monstreSelectionne, objetSelectionne);

      controleur.envoyerCase(ligneSelectionnee, colonneSelectionnee, caseCreee);

      // Remise au propre du modèle de sélection
      label.setText("La case [" + ligneSelectionnee + " , " + colonneSelectionnee + "] a été correctement créée.");

      ligneCarte.getSelectionModel().selectFirst();
      colonneCarte.getSelectionModel().selectFirst();
      monstres.getSelectionModel().selectFirst();
      decors.getSelectionModel().selectFirst();
      objets.getSelectionModel().selectFirst();
   }

   /**
    * Méthode appellée lors du clique sur le bouton "Créer la partie".
    */
   @FXML
   public void creerPartiePressed()
   {
      Stage stageAdmin = (Stage) monstres.getScene().getWindow();

      controleur.fermerConnexionAdmin();
      stageAdmin.close();

      popUpMerci();
   }

   /**
    * Méthode appellée à l'initialisation de la fenêtre
    * @param controleur Le controleur général récupéré de la fenêtre précédente.
    */
   @FXML
   public void setDonnees(final Controleur controleur)
   {
      this.controleur = controleur;

      // Récupération des données contenues dans la base de données
      Task<Void> attendreDebut = new Task<Void>()
      {
         @Override
         public Void call() throws IOException
         {
            listeMonstres = controleur.getListeMonstres().getListe();
            listeDecors = controleur.getListeDecors().getListe();
            listeObjets = controleur.getListeObjets().getListe();
            return null;
         }
      };

      // Quand les données sont récupérées, on set les champs et les listes
      attendreDebut.setOnSucceeded(new EventHandler<WorkerStateEvent>()
      {
         @Override
         public void handle(WorkerStateEvent event)
         {
            monstres.getItems().clear();
            decors.getItems().clear();
            objets.getItems().clear();

            monstres.getItems().addAll(listeMonstres);
            monstres.getItems().add(new Monstre("Vide", null, 0, 0, null));
            decors.getItems().addAll(listeDecors);
            objets.getItems().addAll(listeObjets);
            objets.getItems().add(new Pomme("Vide", 0, null));

            for(int i = 1; i <= 6; i++)
            {
               colonneCarte.getItems().add(new Integer(i));
               ligneCarte.getItems().add(new Integer(i));
            }

            // Sélection par défaut du premier
            ligneCarte.getSelectionModel().selectFirst();
            colonneCarte.getSelectionModel().selectFirst();
            monstres.getSelectionModel().selectFirst();
            decors.getSelectionModel().selectFirst();
            objets.getSelectionModel().selectFirst();
         }
      });

      new Thread(attendreDebut).start();
   }

   /**
    * Méthode privée permettant d'afficher la popup de remerciement de l'admin après la création de la partie.
    */
   private void popUpMerci()
   {
      Stage newStage = (Stage) label.getScene().getWindow();
      BorderPane comp = new BorderPane();

      Image img = new Image("/Images/logoPage.png", 50, 50, true, true);
      ImageView imageView = new ImageView();
      imageView.setImage(img);

      Label info = new Label("Merci d'avoir créé la partie!" +
            "\nVos amis doivent bien s'amuser maintenant");

      info.setTextAlignment(TextAlignment.CENTER);
      info.setTextFill(Color.web("#009900"));

      comp.setCenter(info);
      comp.setStyle("-fx-background-color: #FFFFFF");
      comp.setBottom(imageView);

      Scene stageScene = new Scene(comp, 300, 100);
      newStage.setScene(stageScene);
      newStage.alwaysOnTopProperty();
      newStage.show();
   }
}
