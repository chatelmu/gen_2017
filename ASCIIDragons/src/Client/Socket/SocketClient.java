package Client.Socket;

import java.io.*;
import java.net.Socket;

/**
 * Classe "wrapper" permettant une manipulation facile du socket du client.
 */
public class SocketClient
{
   private String adresse;          // Adresse de connexion saisie par le client
   private int port;                // Port de connexion par défaut décrit dans le protocole
   private Socket socket;           // Le socket wrappé
   private BufferedReader reader;   // Le reader du socket
   private PrintWriter writer;      // Le writer du socket

   /**
    * Constructeur de socketClient, permet d'ouvrir le socket et de récupérer les input/output.
    * @param adresse L'adresse IP du serveur auquel se connecter
    * @param port Le port du serveur sur lequel se connecter
    * @throws IOException Si on a pas réussi à ouvrir le socket
    */
   public SocketClient(String adresse, int port) throws IOException
   {
      this.port = port;
      this.adresse = adresse;

      socket = new Socket(adresse, port);
      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
   }

   /**
    * Méthode wrappée pour automatiquement écrire et flusher le writer du socket.
    * @param str Le contenu à envoyer au serveur
    */
   public void print(String str)
   {
      writer.println(str);
      writer.flush();
   }

   /**
    * Méthode wrappée pour automatiquement lire une réponse du serveur et retourner.
    * @return Le contenu lu dans le socket
    */
   public String read()
   {
      String reponse = "";

      try
      {
          reponse = reader.readLine();
      }
      catch (IOException e)
      {
         System.err.println("Impossible de lire la réponse du serveur : " + e.getMessage());
      }

      return reponse;
   }
}
