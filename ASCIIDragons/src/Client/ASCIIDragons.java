package Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Classe servant de point d'entrée à l'application client.
 */
public class ASCIIDragons extends Application
{
   @Override
   public void start(Stage primaryStage) throws Exception
   {
      Parent login = FXMLLoader.load(getClass().getResource("Vues/VueLogin.fxml"));
      primaryStage.setTitle("ASCII Dragons");
      primaryStage.getIcons().add(new Image("/Images/logoPage.png"));
      primaryStage.setScene(new Scene(login, 492, 328));
      primaryStage.show();
   }

   public static void main(String[] args)
   {
      launch(args);
   }
}
