package Objets;

import java.util.ArrayList;

/**
 * Classe représentant une liste de monstres.
 * La seule utilité de celle-ci est de "wrapper" une ArrayList de monstres pour faciliter la (dé)sérialisation.
 */
public class ListeMonstres
{
   private ArrayList<Monstre> liste;

   /**
    * Constructeur par défaut.
    */
   public ListeMonstres()
   {
      liste = new ArrayList<Monstre>();
   }

   /**
    * Constructeur de base.
    *
    * @param monstres L'ArrayList de monstres à wrapper
    */
   public ListeMonstres(Monstre... monstres)
   {
      this();

      for (Monstre m : monstres)
      {
         liste.add(m);
      }
   }

   /**
    * Méthode permettant d'ajouter un monstre dans la liste.
    *
    * @param monstre
    */
   public void ajouterMonstre(Monstre monstre)
   {
      liste.add(monstre);
   }

   // GETTERS & SETTERS

   public ArrayList<Monstre> getListe()
   {
      return liste;
   }

   public void setListe(ArrayList<Monstre> liste)
   {
      this.liste = liste;
   }
}
