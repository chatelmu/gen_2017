package Objets;

/**
 * Classe abstraite représenant un objet.
 */
public abstract class Objet
{
   private int id;         // L'id de l'objet
   private String nom;     // Le nom de l'objet
   private String type;    // Le type de l'objet (permet de distinguer les classes filles pour la (dé)sérialisation)

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Objet()
   {
   }

   /**
    * Constructeur élaboré.
    *
    * @param nom  Le nom de l'objet
    * @param type Le type de l'objet (arme, armure, joyau, etc...)
    */
   public Objet(String nom, String type)
   {
      this.nom = nom;
      this.type = type;
   }

   /**
    * Construteur élaboré, utilisé par Hibernate.
    *
    * @param id   L'identifiant de l'objet
    * @param nom  Le nom de l'objet
    * @param type Le type de l'objet
    */
   public Objet(int id, String nom, String type)
   {
      this(nom, type);
      this.id = id;
   }

   @Override
   public String toString()
   {
      return nom;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }
}
