package Objets;

/**
 * Classe représentant une case de jeu.
 */
public class Case
{
   private Decor decor;       // Le décors sur la case
   private Monstre monstre;   // Le monstre sur la case
   private Objet objet;       // L'objet sur la case

   /**
    * Constructeur par défaut.
    */
   public Case()
   {
      decor = null;
      monstre = null;
      objet = null;
   }

   /**
    * Constructeur utilisé lors de la création de la carte par défaut. Seul un décors est donné.
    *
    * @param decor Le décor de la case
    */
   public Case(Decor decor)
   {
      this();
      this.decor = decor;
   }

   /**
    * Constructeur élaboré.
    *
    * @param decor   Le decors de la case
    * @param monstre Le monstre sur la case
    * @param objet   L'objet sur la case
    */
   public Case(Decor decor, Monstre monstre, Objet objet)
   {
      this.decor = decor;
      this.monstre = monstre;
      this.objet = objet;
   }

   @Override
   public String toString()
   {
      String m = "";
      String d = "";
      String o = "";

      if (monstre != null)
      {
         m = monstre.getNom();
      }
      if (decor != null)
      {
         d = decor.getNom();
      }
      if (objet != null)
      {
         o = objet.getNom();
      }

      return "Monstre : " + m + "\nDecors : " + d + "\nObjet : " + o;
   }

   /**
    * Méthode permettant de voir si un case est non-initialisée (sans décor).
    *
    * @return True si la case est non-initialisée, sinon false
    */
   public boolean estNoninitialisee()
   {
      return (decor == null);
   }

   // GETTERS & SETTERS

   public Decor getDecor()
   {
      return decor;
   }

   public Monstre getMonstre()
   {
      return monstre;
   }

   public Objet getObjet()
   {
      return objet;
   }

   public void setMonstre(Monstre monstre)
   {
      this.monstre = monstre;
   }

   public void setObjet(Objet objet)
   {
      this.objet = objet;
   }
}
