package Objets;

/**
 * Classe représentant une arme
 */
public class Arme extends Objet
{
   private int attaque;   // Le nombre de dégâts infligés par cette arme

   /**
    * Constructeur spécifique, utilisé par Hibernate.
    *
    * @param nom     Le nom de l'arme
    * @param attaque Le nombre de dégâts infligés par l'arme
    */
   public Arme(String nom, int attaque, String type)
   {
      super(nom, type);
      this.attaque = attaque;
   }

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Arme()
   {
      super();
   }

   // GETTERS & SETTERS

   public int getAttaque()
   {
      return attaque;
   }

   public void setAttaque(int attaque)
   {
      this.attaque = attaque;
   }
}
