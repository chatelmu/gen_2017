package Objets;

/**
 * Classe représentant un monstre
 */
public class Monstre
{
   private int id;         // L'identifiant du monstre
   private String nom;      // Le nom du monstre
   private String image;   // La représentation en ASCII du monstre
   private int pv;         // Le nombre de points de vie du monstre
   private int force;      // Le nombre de degats par attaque que le monstre inflige
   private Type type;      // Le type des dégats infligés

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Monstre()
   {
      super();
   }

   /**
    * Constructeur élaboré.
    *
    * @param nom   Le nom du monstre
    * @param image La représentation ASCII du monstre
    * @param pv    Le nombre de points de vie du monstre
    * @param force La force d'attaque du monstre
    * @param type  Le type de dégâts que le monstre inflige
    */
   public Monstre(String nom, String image, int pv, int force, Type type)
   {
      this.nom = nom;
      this.image = image;
      this.pv = pv;
      this.force = force;
      this.type = type;
   }

   /**
    * Constructeur élaboré, utilisé par Hibernate.
    *
    * @param id    L'identifiant du monstre
    * @param nom   Le nom du monstre
    * @param image La représentation ASCII du monstre
    * @param pv    Le nombre de points de vie du monstre
    * @param force La force d'attaque du monstre
    * @param type  Le type de dégâts que le monstre inflige
    */
   public Monstre(int id, String nom, String image, int pv, int force, Type type)
   {
      this(nom, image, pv, force, type);
      this.id = id;
   }

   @Override
   public String toString()
   {
      return nom;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getImage()
   {
      return image;
   }

   public void setImage(String image)
   {
      this.image = image;
   }

   public int getPv()
   {
      return pv;
   }

   public void setPv(int pv)
   {
      this.pv = pv;
   }

   public int getForce()
   {
      return force;
   }

   public void setForce(int force)
   {
      this.force = force;
   }

   public Type getType()
   {
      return type;
   }

   public void setType(Type type)
   {
      this.type = type;
   }
}
