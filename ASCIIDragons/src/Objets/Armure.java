package Objets;

/**
 * Classe représentant une armure
 *
 * @see Objet
 */
public class Armure extends Objet
{
   private int defenseEau;          // Résistance à l'eau
   private int defenseFeu;          // Résistance au feu
   private int defenseTerre;        // Résistance à la terre
   private int defenseAir;          // Résistance à l'air

   /**
    * Constructeur spécifique, utilisé par Hibernate.
    *
    * @param nom          Le nom de l'armure
    * @param defenseEau   Le niveau de défence envers les dégâts EAU
    * @param defenseFeu   Le niveau de défence envers les dégâts FEU
    * @param defenseTerre Le niveau de défence envers les dégâts TERRE
    * @param defenseAir   Le niveau de défence envers les dégâts AIR
    */
   public Armure(String nom, int defenseEau, int defenseFeu, int defenseTerre, int defenseAir, String type)
   {
      super(nom, type);
      this.defenseEau = defenseEau;
      this.defenseFeu = defenseFeu;
      this.defenseTerre = defenseTerre;
      this.defenseAir = defenseAir;
   }

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Armure()
   {
      super();
   }

   // GETTERS & SETTERS

   public int getDefenseEau()
   {
      return defenseEau;
   }

   public void setDefenseEau(int defenseEau)
   {
      this.defenseEau = defenseEau;
   }

   public int getDefenseFeu()
   {
      return defenseFeu;
   }

   public void setDefenseFeu(int defenseFeu)
   {
      this.defenseFeu = defenseFeu;
   }

   public int getDefenseTerre()
   {
      return defenseTerre;
   }

   public void setDefenseTerre(int defenseTerre)
   {
      this.defenseTerre = defenseTerre;
   }

   public int getDefenseAir()
   {
      return defenseAir;
   }

   public void setDefenseAir(int defenseAir)
   {
      this.defenseAir = defenseAir;
   }
}
