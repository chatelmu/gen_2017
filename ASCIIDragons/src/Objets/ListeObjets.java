package Objets;

import java.util.ArrayList;

/**
 * Classe représentant une liste d'objets.
 * La seule utilité de celle-ci est de "wrapper" une ArrayList d'objets pour faciliter la (dé)sérialisation.
 */
public class ListeObjets
{
   private ArrayList<Objet> liste;

   /**
    * Constructeur par défaut.
    */
   public ListeObjets()
   {
      liste = new ArrayList<Objet>();
   }

   /**
    * Constructeur de base.
    *
    * @param objets L'ArrayList d'objets à wrapper
    */
   public ListeObjets(Objet... objets)
   {
      this();

      for (Objet objet : objets)
      {
         liste.add(objet);
      }
   }

   /**
    * Méthode permettant d'ajouter un objet dans la liste.
    *
    * @param objet L'objet à ajouter
    */
   public void ajouterObjet(Objet objet)
   {
      liste.add(objet);
   }

   // GETTERS & SETTERS

   public ArrayList<Objet> getListe()
   {
      return liste;
   }

   public void setListe(ArrayList<Objet> liste)
   {
      this.liste = liste;
   }
}
