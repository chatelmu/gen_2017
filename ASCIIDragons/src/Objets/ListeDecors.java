package Objets;

import java.util.ArrayList;

/**
 * Classe représentant une liste de décors.
 * La seule utilité de celle-ci est de "wrapper" une ArrayList de décors pour faciliter la (dé)sérialisation.
 */
public class ListeDecors
{
   private ArrayList<Decor> liste;

   /**
    * Constructeur par défaut.
    */
   public ListeDecors()
   {
      liste = new ArrayList<Decor>();
   }

   /**
    * Constructeur de base.
    *
    * @param decors L'ArrayList de décors à wrapper
    */
   public ListeDecors(Decor... decors)
   {
      this();

      for (Decor decor : decors)
      {
         liste.add(decor);
      }
   }

   /**
    * Méthode permettant d'ajouter un décors dans la liste.
    *
    * @param decor Le décors à ajouter
    */
   public void ajouterDecor(Decor decor)
   {
      liste.add(decor);
   }

   // GETTERS & SETTERS

   public ArrayList<Decor> getListe()
   {
      return liste;
   }

   public void setListe(ArrayList<Decor> liste)
   {
      this.liste = liste;
   }
}
