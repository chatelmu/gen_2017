package Objets;

/**
 * Classe représentant un objet de type "pomme".
 * Ce dernier permet au joueur de récupérer des PV.
 */
public class Pomme extends Objet
{
   private int soins;

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Pomme()
   {
      super();
   }

   /**
    * Constructeur élaboré, utilisé par Hibernate.
    *
    * @param nom   Nom de la pomme
    * @param soins Le nombre de PV que la pomme redonne
    * @param type  Le type de la pomme (vaut "pomme")
    */
   public Pomme(String nom, int soins, String type)
   {
      super(nom, type);
      this.soins = soins;
   }

   // GETTERS & SETTERS

   public int getSoins()
   {
      return soins;
   }

   public void setSoins(int soins)
   {
      this.soins = soins;
   }
}
