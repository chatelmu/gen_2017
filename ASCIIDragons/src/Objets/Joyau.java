package Objets;

/**
 * Classe représentant un Joyau (récompense en points)
 */
public class Joyau extends Objet
{
   private int points;   // Le nombre de points pour ce joyau

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Joyau()
   {
      super();
   }

   /**
    * Constructeur élaboré, utilisé par Hibernate.
    *
    * @param nom    Le nom du joyau
    * @param points Le nombre de points que représente ce joyau
    */
   public Joyau(String nom, int points, String type)
   {
      super(nom, type);
      this.points = points;
   }

   // GETTERS & SETTERS

   public int getPoints()
   {
      return points;
   }

   public void setPoints(int points)
   {
      this.points = points;
   }
}
