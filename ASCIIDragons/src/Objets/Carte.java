package Objets;

import Protocole.Initialisations;
import Serveur.Hibernate.ServicesSQL;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Classe représentant la carte du jeu. Une carte est une collection de cases.
 *
 * @see Case
 */
public class Carte
{
   private ArrayList<Case> cases;   // La collection de cases

   /**
    * Constructeur par défaut de Carte
    */
   public Carte()
   {
      cases = new ArrayList<Case>();
   }

   /**
    * Constructeur élaboré de carte. Les cases sont initialisées avec un décor aléatoire parmis les décors disponibles.
    *
    * @param taille La taille de la carte à créer
    */
   public Carte(int taille)
   {
      this();

      // On récupère les décors pour pouvoir faire de l'aléatoire
      ServicesSQL servicesSQL = new ServicesSQL();
      List<Decor> listeDecors = servicesSQL.getListeDecors();

      // On crée les cases mais on met rien dedans -> carte "par défaut" donc toutes les cases vides
      for (int i = 0; i < taille; i++)
      {
         // On récupère un nombre aléatoire pour choisir le décors
         int randomDecor = ThreadLocalRandom.current().nextInt(0, listeDecors.size());

         cases.add(new Case(listeDecors.get(randomDecor)));
      }
   }

   @Override
   public String toString()
   {
      StringBuffer reponse = new StringBuffer();

      for (int i = 0; i < cases.size(); i++)
      {
         int ligne = (i / Initialisations.COTE_CARTE) + 1;
         int colonne = (i % Initialisations.COTE_CARTE) + 1;

         reponse.append("Case l" + ligne + ", c" + colonne + " : \n");

         if (cases.get(i).estNoninitialisee())
         {
            reponse.append("vide\n");
         }
         else
         {
            reponse.append(cases.get(i) + "\n");
         }
      }

      return new String(reponse);
   }

   // GETTERS & SETTERS

   public ArrayList<Case> getCases()
   {
      return cases;
   }

   public Case getCase(int coordX, int coordY)
   {
      return cases.get((coordY - 1) * Initialisations.COTE_CARTE + (coordX - 1));
   }
}
