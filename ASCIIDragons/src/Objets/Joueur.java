package Objets;

import Protocole.Initialisations;

import java.io.*;
import java.net.Socket;

/**
 * Classe représentant un Joueur (humain)
 */
public class Joueur
{
   private String nom;              // Le nom du joueur
   private double pvMax;            // Le nombre de points de vie maximum du joueur
   private double pvActuels;        // Le nombre de PV actuels du joueur
   private int defenseAir;          // La résistance à l'air du joueur
   private int defenseTerre;        // La résistance à la terre du joueur
   private int defenseEau;          // La résistance à l'eau du joueur
   private int defenseFeu;          // La résistance au feu du joueur
   private int attaque;             // Le nombre de dégâts que le joueur inflige
   private String img;              // La représentation ASCII du joueur
   private int score;               // Le score actuel du joueur
   private Socket socket;           // Le socket par lequel joindre le joueur
   private BufferedReader reader;   // Le reader du socket du joueur
   private PrintWriter writer;      // Le writer du socket du joueur
   private int coordX, coordY;      // Les coordonnées actuelles du joueur

   /**
    * Constructeur élaboré pour la création d'un joueur par le serveur.
    *
    * @param nom    Le nom du joueur
    * @param socket Le socket du joueur
    * @param coordX La coordonnée X du joueur
    * @param coordY Le coordonnée Y du joueur
    */
   public Joueur(String nom, Socket socket, int coordX, int coordY)
   {
      this.nom = nom;
      this.socket = socket;
      this.coordX = coordX;
      this.coordY = coordY;

      try
      {
         this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

      }
      catch (IOException e)
      {
         System.out.println("Erreur lors de la récupération des streams");
      }

      // Initialisation par défaut selon le fichier de configuration
      this.pvMax = Initialisations.PV_JOUEUR_MAX;
      this.pvActuels = Initialisations.PV_JOUEUR_INIT;
      this.defenseAir = Initialisations.DEFENSE_AIR_INIT;
      this.defenseEau = Initialisations.DEFENSE_EAU_INIT;
      this.defenseFeu = Initialisations.DEFENSE_FEU_INIT;
      this.defenseTerre = Initialisations.DEFENSE_TERRE_INIT;
      this.attaque = Initialisations.ATTAQUE_JOUEUR_INIT;
      score = 0;
   }

   /**
    * Constructeur élaboré pour la création d'un admin par le serveur.
    *
    * @param nom    Le nom de l'admin (non utilisé)
    * @param socket Le socket de l'admin
    */
   public Joueur(String nom, Socket socket)
   {
      this.socket = socket;
      this.nom = nom;
   }

   // GETTERS & SETTERS

   public Socket getSocket()
   {
      return socket;
   }

   public BufferedReader getReader()
   {
      return reader;
   }

   public PrintWriter getWriter()
   {
      return writer;
   }

   public int getCoordX()
   {
      return coordX;
   }

   public int getCoordY()
   {
      return coordY;
   }

   public void setCoordX(int coordX)
   {
      this.coordX = coordX;
   }

   public void setCoordY(int coordY)
   {
      this.coordY = coordY;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public double getPvMax()
   {
      return pvMax;
   }

   public void setPvMax(double pvMax)
   {
      this.pvMax = pvMax;
   }

   public double getPvActuels()
   {
      return pvActuels;
   }

   public void setPvActuels(double pvActuels)
   {
      this.pvActuels = pvActuels;
   }

   public int getDefenseAir()
   {
      return defenseAir;
   }

   public void setDefenseAir(int defenseAir)
   {
      this.defenseAir = defenseAir;
   }

   public int getDefenseTerre()
   {
      return defenseTerre;
   }

   public void setDefenseTerre(int defenseTerre)
   {
      this.defenseTerre = defenseTerre;
   }

   public int getDefenseEau()
   {
      return defenseEau;
   }

   public void setDefenseEau(int defenseEau)
   {
      this.defenseEau = defenseEau;
   }

   public int getDefenseFeu()
   {
      return defenseFeu;
   }

   public void setDefenseFeu(int defenseFeu)
   {
      this.defenseFeu = defenseFeu;
   }

   public int getAttaque()
   {
      return attaque;
   }

   public void setAttaque(int attaque)
   {
      this.attaque = attaque;
   }

   public int getScore()
   {
      return score;
   }

   public void setScore(int score)
   {
      this.score = score;
   }
}
