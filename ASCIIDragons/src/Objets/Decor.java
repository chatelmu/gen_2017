package Objets;

/**
 * Classe représentant un décor en ASCII Art
 */
public class Decor
{
   private int id;         // L'id du décor
   private String image;   // La représentation ASCII du décor
   private String nom;     // Le nom du décor

   /**
    * Constructeur par défaut, nécessaire pour Hibernate.
    */
   public Decor()
   {
   }

   /**
    * Constructeur élaboré.
    *
    * @param image La représentation en ASCII art du décor
    * @param nom   Le nom du décor
    */
   public Decor(String image, String nom)
   {
      this.image = image;
      this.nom = nom;
   }

   /**
    * Constructeur élaboré, utilisé par Hibernate.
    *
    * @param id    l'identifiant du décor
    * @param image la représentation en ASCII art du décor
    * @param nom   le nom du décor
    */
   public Decor(int id, String image, String nom)
   {
      this(image, nom);
      this.id = id;
   }

   @Override
   public String toString()
   {
      return nom;
   }

   // GETTERS & SETTERS

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getImage()
   {
      return image;
   }

   public void setImage(String image)
   {
      this.image = image;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }
}
