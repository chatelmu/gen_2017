package Objets;

/**
 * Enum représentant le type de dommage que les monstres peuvent infliger.
 */
public enum Type
{
   FEU,
   EAU,
   AIR,
   TERRE
}
