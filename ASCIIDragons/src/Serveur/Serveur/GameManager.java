package Serveur.Serveur;

import Objets.Carte;
import Objets.Case;
import Objets.Joueur;
import Protocole.Initialisations;
import Protocole.ProtocoleASCIIDragons;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Le Game Manager connaît la carte, le joueur qui joue, la liste des joueurs, etc...
 * Il permet de faire fonctionner la partie, c'est le coeur de la gestion du jeu.
 */
public class GameManager
{
   private Carte carte;                      // La carte du jeu
   private ArrayList<Joueur> listeJoueur;    // La liste des joueurs
   private ArrayList<Case> casesOccupees;    // Collection de cases occupées par les joueurs
   private Joueur administrateur;            // L'administrateur de la partie
   private int compteurTours;                // Nombre de tours joués
   private boolean jeuEnCours;               // Booléen permettant de déterminer si le jeu est en cours ou pas
   private int joueurActif;                  // L'index du joueur qui joue actuellement

   /**
    * Constructeur par défaut
    */
   public GameManager()
   {
      carte = new Carte(Initialisations.TAILLE_CARTE);
      listeJoueur = new ArrayList<Joueur>();
      casesOccupees = new ArrayList<Case>();
      compteurTours = 1;
      jeuEnCours = false;
      joueurActif = 0;
   }

   /**
    * Méthode signalant au joueur que c'est son tour et mettre à jour le joueurActif et gérer la fin du jeu
    */
   public void prochainJoueur()
   {
      listeJoueur.get(joueurActif).getWriter().println(ProtocoleASCIIDragons.CMD_TOUR_JOUEUR);
      listeJoueur.get(joueurActif).getWriter().flush();
      joueurActif = (joueurActif + 1) % listeJoueur.size();

      // Si tous les joueurs ont fait un tour
      if (joueurActif == 0)
      {
         compteurTours++;
      }

      // Si on a atteint la limite de tours
      // TODO ça ne marche pas très bien encore
      if (compteurTours == Initialisations.NOMBRE_DE_TOURS)
      {
         envoyerScores();
         jeuEnCours = false;
      }
   }

   /**
    * Méthode permetant d'envoyer les scores à chacun en fin de partie.
    */
   public void envoyerScores()
   {
      String resultat = preparerScores();

      for (Joueur j : listeJoueur)
      {
         j.getWriter().println(ProtocoleASCIIDragons.CMD_FIN_PARTIE);
         j.getWriter().flush();
         j.getWriter().println(resultat);
         j.getWriter().flush();
         try
         {
            j.getSocket().close();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
      }
   }

   /**
    * Methode qui prépare les scores
    */
   public String preparerScores()
   {
      StringBuilder stringBuilder = new StringBuilder();

      for (Joueur j : listeJoueur)
      {
         stringBuilder.append(j.getNom() + " : " + j.getScore() + " | ");
      }

      return stringBuilder.toString();
   }

   // GETTERS & SETTERS

   public void ajouterJoueur(Joueur joueur)
   {
      listeJoueur.add(joueur);
   }

   public void retirerJoueur(Joueur joueur)
   {
      listeJoueur.remove(joueur);
   }

   public Carte getCarte()
   {
      return carte;
   }

   public ArrayList<Joueur> getListeJoueur()
   {
      return listeJoueur;
   }

   public Joueur getAdministrateur()
   {
      return administrateur;
   }

   public void setAdministrateur(Joueur administrateur)
   {
      this.administrateur = administrateur;
   }

   public ArrayList<Case> getCasesOccupees()
   {
      return casesOccupees;
   }

   public boolean isJeuEnCours()
   {
      return jeuEnCours;
   }

   public void setJeuEnCours(boolean jeuEnCours)
   {
      this.jeuEnCours = jeuEnCours;
   }
}
