package Serveur.Serveur;

import Objets.*;
import Protocole.ProtocoleASCIIDragons;
import Protocole.RequeteServeur;
import Serveur.Hibernate.ServicesSQL;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de traiter les requêtes de l'administrateur lors de la phase de création de la carte
 */
public class TraitementAdministrateur
{
   private PrintWriter writer = null;
   private BufferedReader reader = null;
   private Gson gson = null;
   private Carte carte;

   /**
    * Constructeur élaboré.
    *
    * @param writer Le flux de sortie du socket de l'administrateur
    * @param reader La flux d'entrée du socket de l'administrateur
    * @param gson L'instance gson permettant la (dé)sérialisation
    * @param carte La carte que va personnaliser gentiment l'administrateur
    */
   public TraitementAdministrateur(PrintWriter writer, BufferedReader reader, Gson gson, Carte carte)
   {
      this.writer = writer;
      this.reader = reader;
      this.gson = gson;
      this.carte = carte;
   }

   /**
    * Permet l'envoi de informations relatives au plateau de jeu.
    */
   public void envoyerInformationsObjets()
   {
      ServicesSQL servicesSQL = new ServicesSQL();

      ListeObjets listeObjets = new ListeObjets();
      List<Arme> listeArme = servicesSQL.getListeArmes();
      List<Pomme> listePomme = servicesSQL.getListePommes();
      List<Armure> listeArmure = servicesSQL.getListeArmures();
      List<Joyau> listeJoyau = servicesSQL.getListeJoyaux();

      // Ajouter les armes aux objets
      for (Objet o : listeArme)
         listeObjets.ajouterObjet(o);

      // Ajouter les armures aux objets
      for (Objet o : listeArmure)
         listeObjets.ajouterObjet(o);

      // Ajouter les pommes aux objets
      for (Objet o : listePomme)
         listeObjets.ajouterObjet(o);

      // Ajouter les joyaux aux objets
      for (Objet o : listeJoyau)
         listeObjets.ajouterObjet(o);

      ListeMonstres listeMonstres = new ListeMonstres();
      listeMonstres.setListe((ArrayList<Monstre>)servicesSQL.getListeMonstres());
      writer.println(gson.toJson(listeMonstres));

      ListeDecors listeDecors = new ListeDecors();
      listeDecors.setListe((ArrayList<Decor>)servicesSQL.getListeDecors());
      writer.println(gson.toJson(listeDecors));

      writer.println(gson.toJson(listeObjets));

      /*
      // Ces quelques lignes étaient utilisées pour les tests avant l'établissement de la base de données.
      // Envoyer les données relatives aux monstres
      writer.println(gson.toJson(new ListeMonstres(new Monstre("Dinosaure", ASCIIArt.monstre1, 10, 1, Type.TERRE),
              new Monstre("Dracula", ASCIIArt.monstre2, 100, 2, Type.AIR),
              new Monstre("Squelette", ASCIIArt.monstre3, 55, 1, Type.FEU))));

      // Envoyer les données relatives aux différents décors
      writer.println(gson.toJson(new ListeDecors(new Decor(ASCIIArt.decors1, "Nuage"),
              new Decor(ASCIIArt.decors2, "Plage"),
              new Decor(ASCIIArt.decors3, "Camping"))));

      // Envoyer les objets (c'est un peu spécial à cause du polymorphisme)
      writer.println(gson.toJson(new ListeObjets(new Joyau("Joyau", 20, "Joyau"),
            new Pomme("Pomme", 100, "Pomme"),
            new Arme("Tertre des âmes", 3, "Arme"),
            new Armure("Armure de flamme", 0, 10, 0, 0, "Armure"))));
      */
   }

   /**
    * Méthode permettant le traitement des requêtes de l'administrateur durant la phase de création de la carte.
    *
    * @throws IOException En cas de problème de lecture du contenu des sockets
    */
   public void creationCarte() throws IOException
   {
      boolean enConstruction = true;
      ConstructeurCarte constructeurCarte = new ConstructeurCarte(carte);
      RequeteServeur requeteServeur;

      // Tant qu'on continue a recevoir des informations et que la carte est en construction
      while (enConstruction && (requeteServeur = gson.fromJson(reader.readLine(), RequeteServeur.class)) != null)
      {
         switch (requeteServeur.getTypeRequete())
         {
            // On veut configurer une case
            case ProtocoleASCIIDragons.CMD_CONFIGURER_CASE:
            {
               int ligne = Integer.parseInt(requeteServeur.getArguments()[0]);
               int colonne = Integer.parseInt(requeteServeur.getArguments()[1]);
               Case nouvelleCase = gson.fromJson(requeteServeur.getArguments()[2], Case.class);

               System.out.println("La case " + ligne + ", " + colonne + " a bien été recue.\n" + nouvelleCase);
               constructeurCarte.configurerCase(ligne, colonne, nouvelleCase);
               System.out.println("\nCarte mise à jour : \n" + carte);
               break;
            }

            // On a terminé de configurer la carte
            case ProtocoleASCIIDragons.CMD_FERMER_SOCKET:
            {
               enConstruction = false;
               break;
            }
         }
      }
   }
}
