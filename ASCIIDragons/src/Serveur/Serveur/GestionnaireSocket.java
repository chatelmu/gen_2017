package Serveur.Serveur;

import Serveur.Utilitaires.Journalisation;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Classe permettant la gestion du socket associé au client.
 */
public class GestionnaireSocket implements Runnable
{
   private Socket socket;                          // Le socket du client
   private GestionnaireClient gestionnaireClient;  // Le gestionnaireClient associé à ce client
   private InputStream is;                         // Le flux d'entrée du socket
   private OutputStream os;                        // Le flux de sortie du socket
   private Serveur serveur;                        // Le serveur

   /**
    * Constructeur élaboré.
    *
    * @param serveur Le serveur
    * @param socket  Le socket associé au client à gérer
    */
   public GestionnaireSocket(Serveur serveur, Socket socket)
   {
      this.socket = socket;
      this.serveur = serveur;

      try
      {
         // Récupération des streams d'entrée et de sortie
         is = socket.getInputStream();
         os = socket.getOutputStream();
      }
      catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur lors de la création du gestionnaire de socket : " + e.getMessage());
      }

      // Création du GestionnaireClient
      gestionnaireClient = new GestionnaireClient(is, os, serveur);
   }

   /**
    * Méthode lancée au lancement du thread client. Traite les requêtes du client. Quand ce dernier
    * a terminé, on ferme les flux I/O et le socket.
    */
   public void run()
   {
      try
      {
         // Tant que le client fait des requêtes, les traiter
         gestionnaireClient.traiterRequetes(socket);

      }
      catch (IOException e)
      {
         Journalisation.getERR().severe("Erreur dans le traitement des requêtes : " + e.getMessage());
      }
      finally
      {
         try
         {
            // On se retire du pool de clients
            serveur.supprimerSocket(socket);

            // Fermer le socket
            socket.close();
         }
         catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur dans le gestionnaire de socket lors de la fermeture / " +
                                                 "retrait de la liste des sockets ouverts dans le serveur : " +
                                                 e.getMessage());
         }
      }
   }
}
