package Serveur.Serveur;

import Objets.Joueur;
import Objets.ListeObjets;
import Objets.Objet;
import Protocole.ProtocoleASCIIDragons;
import Protocole.RequeteConnexion;
import Protocole.Initialisations;
import Utilitaires.GsonListeObjetsAdapter;
import Utilitaires.GsonObjetAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.net.Socket;

/**
 * Classe permettant la gestion d'un client. Dans cette classe, nous avons accès aux différents
 * flux d'entrée et de sortie vers le client, un objet GSON afin de sérialiser et désérialiser les
 * requêtes et les réponses.
 * Les requêtes possibles depuis un client sont décrites dans le protocole (Protocoles.ProtocoleWorksOn)
 */
public class GestionnaireClient
{
   private final static Integer synchro = 1;    // Objet de synchronisation entre clients
   private BufferedReader reader;               // Reader du socket de ce client
   private PrintWriter writer;                  // Writer du socket de ce client
   private Serveur serveur;                     // Référence au serveur
   private Gson gson;                           // Instance gson pour la (dé)sérialisation

   /**
    * Constructeur élaboré.
    *
    * @param is      Flux d'entrée du socket de ce client
    * @param os      Flux de sortie du socket de ce client
    * @param serveur Le serveur
    */
   public GestionnaireClient(InputStream is, OutputStream os, Serveur serveur)
   {
      // Création des canaux de communication
      writer = new PrintWriter(new OutputStreamWriter(os), true);
      reader = new BufferedReader(new InputStreamReader(is));
      this.serveur = serveur;

      // Création du gson et mise en place des ses adaptateurs pour ListeObjets et Objets
      GsonBuilder gsonBuilder = new GsonBuilder();
      gsonBuilder.registerTypeAdapter(ListeObjets.class, new GsonListeObjetsAdapter());
      gsonBuilder.registerTypeAdapter(Objet.class, new GsonObjetAdapter());
      this.gson = gsonBuilder.create();
   }

   /**
    * Fonction qui permet de traiter le client en répondant à ses requêtes. Premièrement, le client se voit attribuer
    * son rôle (admin ou joueur) selon son arrivée sur le serveur. Suivant ce rôle, les requêtes du client sont traitées.
    *
    * @throws IOException Si des problèmes de lectures surviennent
    */
   public void traiterRequetes(Socket socket) throws IOException
   {
      String commande;

      // Gestion de la connexion du client
      RequeteConnexion requeteConnexion = gson.fromJson(reader.readLine(), RequeteConnexion.class);

      // Si on a bien recu un login
      if (requeteConnexion.getLogin() != null)
      {
         // Si on a pas encore un admin, ce client devient l'admin
         if (serveur.getGm().getAdministrateur() == null)
         {
            synchronized (synchro)
            {
               // On ajoute l'administrateur dans le game manager
               serveur.getGm().setAdministrateur(new Joueur(requeteConnexion.getLogin(), socket));

               // On notifie le client qu'il est l'administrateur
               writer.println("admin");

               // Création de l'objet permettant le traitement des commandes de l'admin
               TraitementAdministrateur traitementAdministrateur = new TraitementAdministrateur(writer, reader, gson, serveur.getGm().getCarte());

               // On envoie les informations sur les objets
               traitementAdministrateur.envoyerInformationsObjets();

               // On traite les commandes pour la création de la carte
               traitementAdministrateur.creationCarte();

               serveur.getGm().setJeuEnCours(true);
            }
         }
         // Autrement on a déjà un administrateur, c'est donc un futur joueur
         else
         {
            // Ajouter le joueur à la liste des joueurs
            int numeroJoueur = serveur.getGm().getListeJoueur().size();
            Joueur joueur = null;

            // Déterminer la position de départ du joueur ou si le serveur est plein
            switch (numeroJoueur)
            {
               case 0:
                  joueur = new Joueur(requeteConnexion.getLogin(), socket, 1, 1);
                  System.out.println("nouveau joueur : " + joueur.getCoordX() + ", " + joueur.getCoordY());
                  serveur.getGm().getCasesOccupees().add(serveur.getGm().getCarte().getCase(1, 1));
                  break;
               case 1:
                  joueur = new Joueur(requeteConnexion.getLogin(), socket, Initialisations.COTE_CARTE, Initialisations.COTE_CARTE);
                  System.out.println("nouveau joueur : " + joueur.getCoordX() + ", " + joueur.getCoordY());
                  serveur.getGm().getCasesOccupees().add(serveur.getGm().getCarte().getCase(Initialisations.COTE_CARTE, Initialisations.COTE_CARTE));
                  break;
               case 2:
                  joueur = new Joueur(requeteConnexion.getLogin(), socket, 1, Initialisations.COTE_CARTE);
                  System.out.println("nouveau joueur : " + joueur.getCoordX() + ", " + joueur.getCoordY());
                  serveur.getGm().getCasesOccupees().add(serveur.getGm().getCarte().getCase(1, Initialisations.COTE_CARTE));
                  break;
               case 3:
                  joueur = new Joueur(requeteConnexion.getLogin(), socket, Initialisations.COTE_CARTE, 1);
                  System.out.println("nouveau joueur : " + joueur.getCoordX() + ", " + joueur.getCoordY());
                  serveur.getGm().getCasesOccupees().add(serveur.getGm().getCarte().getCase(Initialisations.COTE_CARTE, 1));
                  break;
               default:
                  writer.println("full");
                  return;
            }

            // Ajouter le joueur a la liste des joueurs actifs
            serveur.getGm().ajouterJoueur(joueur);

            // Notifier le client que c'est un joueur
            writer.println("joueur");

            // On se prépare pour les commandes de traitement du joueur
            TraitementJoueur traitementJoueur = new TraitementJoueur(writer, reader, gson, joueur, serveur);

            synchronized (synchro)
            {
               // Signaler le début de la partie
               traitementJoueur.signalerDebutPartie();
            }

            if (joueur.getCoordX() == 1 && joueur.getCoordY() == 1)
            {
               serveur.getGm().prochainJoueur();
            }
            else
            {
               joueur.getWriter().println(ProtocoleASCIIDragons.CMD_PAS_TON_TOUR);
               joueur.getWriter().flush();
            }

            // Le joueur joue
            traitementJoueur.jouer();
         }
      }
   }
}

