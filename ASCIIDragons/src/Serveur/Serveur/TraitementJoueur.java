package Serveur.Serveur;

import Objets.*;
import Protocole.*;
import Protocole.Initialisations;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Classe permettant de traiter les requêtes des joueurs durant une partie.
 */
public class TraitementJoueur
{
   private PrintWriter writer;      // Flux de sortie du socket du joueur
   private BufferedReader reader;   // Flux d'entrée du socket du joueur
   private Gson gson;               // Instance du gson pour la (dé)sérialisation
   private Joueur joueur;           // L'objet représentant le joueur
   private Serveur serveur;         // Le serveur
   private Random random;           // Générateur de nombres aléatoires

   /**
    * Constructeur élaboré
    *
    * @param writer Le writer du socket
    * @param reader Le reader du socket
    * @param gson   Le moteur gson pour la sérialisation / désérialisation
    */
   public TraitementJoueur(PrintWriter writer, BufferedReader reader, Gson gson, Joueur joueur, Serveur serveur)
   {
      this.writer = writer;
      this.reader = reader;
      this.gson = gson;
      this.joueur = joueur;
      this.serveur = serveur;
      random = new Random();
   }

   /**
    * Methode qui informe notre joueur que la partie a commencé.
    */
   public void signalerDebutPartie()
   {
      writer.println(Integer.toString(ProtocoleASCIIDragons.CMD_DEBUT_PARTIE));
   }

   /**
    * La boucle principale qui traite le joueur tant que la partie est en cours.
    *
    * @throws IOException En cas de problème lors de la lecture dans le socket
    */
   public void jouer() throws IOException
   {
      // Enovoyer la premiere case au joueur (celle sur laquelle il pop)
      ReponsePossibiliteDeplacement reponsePossibiliteDeplacement = calculerPossibilitesDeplacement();
      writer.println(gson.toJson(reponsePossibiliteDeplacement));

      while (serveur.getGm().isJeuEnCours())
      {
         // Recevoir la requete
         String requete = reader.readLine();
         RequeteServeur requeteServeur = gson.fromJson(requete, RequeteServeur.class);

         // Switch selon la requête
         switch (requeteServeur.getTypeRequete())
         {
            // On veut se déplacer
            case ProtocoleASCIIDragons.CMD_DEMANDE_DEPLACEMENT:
               deplacer(Integer.parseInt(requeteServeur.getArguments()[0]));        // Récupérer le code de déplacement
               reponsePossibiliteDeplacement = calculerPossibilitesDeplacement();   // Déterminer le prochain déplacement possible
               writer.println(gson.toJson(reponsePossibiliteDeplacement));          // Renseigner le joueur
               break;

            // On attaque le monstre
            case ProtocoleASCIIDragons.CMD_ATTAQUER:
               attaquer();
               break;

            // On tente de fuir
            case ProtocoleASCIIDragons.CMD_FUIR:
               boolean reuissite = fuir();
               writer.println(String.valueOf(reuissite));             // Renvoi si oui ou non il a pu fuir (boolean);

               // Si il a pu fuir, renvoyer la nouvelle case
               if (reuissite)
               {
                  writer.println(gson.toJson(calculerPossibilitesDeplacement()));
               }
               else
               {
                  writer.println(String.valueOf(joueur.getPvActuels()));
               }
               break;

            //Ramasser un objet
            case ProtocoleASCIIDragons.CMD_RAMASSER_OBJET:
               ramasserObjet();
               break;
         }

         // Lancer le joueur suivant
         serveur.getGm().prochainJoueur();
      }
   }

   /**
    * Méthode permettant de ramasser un objet.
    */
   public void ramasserObjet()
   {
      // On récupère les stats de l'objet et on les ajoute au joueur
      Objet objet = serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()).getObjet();

      // On détermine quoi faire selon le type d'objet
      if (objet instanceof Arme)
      {
         joueur.setAttaque(((Arme) objet).getAttaque());
      }
      else if (objet instanceof Armure)
      {
         joueur.setDefenseAir(((Armure) objet).getDefenseAir());
         joueur.setDefenseEau(((Armure) objet).getDefenseEau());
         joueur.setDefenseFeu(((Armure) objet).getDefenseFeu());
         joueur.setDefenseTerre(((Armure) objet).getDefenseTerre());
      }
      else if (objet instanceof Pomme)
      {
         joueur.setPvActuels(joueur.getPvActuels() + ((Pomme) objet).getSoins());

         // Si on dépasse la vie max, plafonner
         if (joueur.getPvActuels() > joueur.getPvMax())
         {
            joueur.setPvActuels(joueur.getPvMax());
         }
      }
      else if (objet instanceof Joyau)
      {
         joueur.setScore(joueur.getScore() + ((Joyau) objet).getPoints());
      }

      // On enleve l'objet de la case
      serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()).setObjet(null);

      // On envoie le nouvel état du joueur (pv à -1000 car le pv du monstre n'ont pas changés)
      writer.println(gson.toJson(new ReponseEtatFinDeTour(new ReponseEtatJoueur(joueur), -1000)));
   }

   /**
    * Permet d'attaquer le monstre sur la case courante.
    */
   private void attaquer()
   {
      Monstre monstre = serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()).getMonstre();
      double pvJoueur = joueur.getPvActuels();

      // On inflige les dégâts au monstre
      monstre.setPv(monstre.getPv() - joueur.getAttaque());

      // Si le monstre est mort
      if (monstre.getPv() <= 0)
      {
         // Supprimer le monstre de la case
         serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()).setMonstre(null);
      }
      // Autrement il est encore en vie, il riposte
      else
      {
         repliqueMonstre();
      }

      // Notifier le client de l'issue de la bataille
      ReponseEtatFinDeTour etatFinDeTour = new ReponseEtatFinDeTour(new ReponseEtatJoueur(joueur), monstre.getPv());
      writer.println(gson.toJson(etatFinDeTour));
   }

   /**
    * Méthode privée permettant de calculer le nouveau nombre de point de vie du joueur après la vengeance du monstre.
    */
   private void repliqueMonstre()
   {
      Monstre monstre = serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()).getMonstre();
      double pvJoueur = joueur.getPvActuels();

      switch (monstre.getType())
      {
         case AIR:
            pvJoueur -= ((monstre.getForce() - (monstre.getForce() * joueur.getDefenseAir() / 100)));
            break;
         case EAU:
            pvJoueur -= ((monstre.getForce() - (monstre.getForce() * joueur.getDefenseEau() / 100)));
            break;
         case FEU:
            pvJoueur -= ((monstre.getForce() - (monstre.getForce() * joueur.getDefenseFeu() / 100)));
            break;
         case TERRE:
            pvJoueur -= ((monstre.getForce() - (monstre.getForce() * joueur.getDefenseTerre() / 100)));
            break;
      }

      joueur.setPvActuels(pvJoueur);
   }

   /**
    * Permet de tenter de fuir.
    *
    * @return Si le joueur à pu fuir
    */
   private boolean fuir()
   {
      int essai = random.nextInt(101); // Calcul d'un nombre aléatoire entre 0 et 100
      int choix = 0;

      // Si le joueur a pu fuir, sauter a une case valide aléatoire (si entre 80 et 100) -> 20% de change de fuir
      if (essai >= 80)
      {
         boolean rechercheSaut = true;
         ReponsePossibiliteDeplacement possibilitesDeplacement = calculerPossibilitesDeplacement();

         // Trouver un choix possible de déplacement
         while (rechercheSaut)
         {
            choix = random.nextInt(4); // Nombre aléatoire entre 0 et 3 compris

            switch (choix)
            {
               case 0:
                  rechercheSaut = !possibilitesDeplacement.getNord();
                  break;
               case 1:
                  rechercheSaut = !possibilitesDeplacement.getSud();
                  break;
               case 2:
                  rechercheSaut = !possibilitesDeplacement.getEst();
                  break;
               case 3:
                  rechercheSaut = !possibilitesDeplacement.getOuest();
                  break;
            }
         }

         // On saute au nouvel emplacement et on retourne
         deplacer(choix);
         return true;
      }
      // Il n'a pas pu fuir, le faire souffrir MOUAHAHA
      else
      {
         // Le monstre est pas content du tout qu'on veuille le laisser tout seul, alors il réplique
         repliqueMonstre();
         return false;
      }
   }

   /**
    * Déplacer un joueur sur une autre case. Le test de la validité de la case doit être effectué auparavant.
    *
    * @param posX La nouvelle position X du joueur
    * @param posY La nouvelle position Y du joueur
    * @See calculerPossibilitesDeplacement
    */
   private void deplacer(int posX, int posY)
   {
      // Supprimer l'ancienne case occupée
      serveur.getGm().getCasesOccupees().remove(serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()));

      // Deplacer le joueur
      joueur.setCoordX(posX);
      joueur.setCoordY(posY);

      // Mettre la nouvelle case occupée dans la liste
      serveur.getGm().getCasesOccupees().add(serveur.getGm().getCarte().getCase(posX, posY));
   }

   /**
    * Effectuer un déplacement dans une direction donnée.
    *
    * @param direction La direction dans laquelle se déplacer
    */
   private void deplacer(int direction)
   {
      switch (direction)
      {
         // Deplacement au nord
         case 0:
            deplacer(joueur.getCoordX(), joueur.getCoordY() - 1);
            break;

         // Deplacement au sud
         case 1:
            deplacer(joueur.getCoordX(), joueur.getCoordY() + 1);
            break;

         // Deplacement à l'est
         case 2:
            deplacer(joueur.getCoordX() + 1, joueur.getCoordY());
            break;

         // Deplacement à l'ouest
         case 3:
            deplacer(joueur.getCoordX() - 1, joueur.getCoordY());
            break;
      }
   }

   /**
    * Permet de déterminer quels déplacements sont possibles sur la case actuelle.
    *
    * @return La reponse a la demande
    */
   private ReponsePossibiliteDeplacement calculerPossibilitesDeplacement()
   {
      int posX, posY;
      posX = joueur.getCoordX();
      posY = joueur.getCoordY();

      ReponsePossibiliteDeplacement reponsePossibiliteDeplacement = new ReponsePossibiliteDeplacement(serveur.getGm().getCarte().getCase(joueur.getCoordX(), joueur.getCoordY()));

      // Check a gauche
      if (posX - 1 >= 1)
      {
         // Si la case n'est pas occupée
         if (!serveur.getGm().getCasesOccupees().contains(serveur.getGm().getCarte().getCase(posX - 1, posY)))
         {
            // Deplacement gauche possible
            reponsePossibiliteDeplacement.setOuest(true);
         }
      }

      // Check a droite
      if (posX + 1 <= Initialisations.COTE_CARTE)
      {
         // Si la case n'est pas occupée
         if (!serveur.getGm().getCasesOccupees().contains(serveur.getGm().getCarte().getCase(posX + 1, posY)))
         {
            // Deplacement droite possible
            reponsePossibiliteDeplacement.setEst(true);
         }
      }

      // Check en bas
      if (posY + 1 <= Initialisations.COTE_CARTE)
      {
         // Si la case n'est pas occupée
         if (!serveur.getGm().getCasesOccupees().contains(serveur.getGm().getCarte().getCase(posX, posY + 1)))
         {
            // Deplacement bas possible
            reponsePossibiliteDeplacement.setSud(true);
         }
      }

      // Check en haut
      if (posY - 1 >= 1)
      {
         // Si la case n'est pas occupée
         if (!serveur.getGm().getCasesOccupees().contains(serveur.getGm().getCarte().getCase(posX, posY - 1)))
         {
            // Deplacement haut possible
            reponsePossibiliteDeplacement.setNord(true);
         }
      }

      return reponsePossibiliteDeplacement;
   }
}
