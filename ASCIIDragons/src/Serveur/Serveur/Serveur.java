package Serveur.Serveur;

import Serveur.Hibernate.ServicesSQL;
import Serveur.Utilitaires.Journalisation;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Serveur de ASCII DRANGONS, tant que le serveur n'est pas arrêté, traiter des clients.
 * Si on demande l'arrêt du serveur, déconnecter tous les clients.
 */
public class Serveur implements Runnable
{
   private List<Socket> poolSocket;   // La liste de tous les sockets ouverts vers des clients
   private GameManager gm;             // Le game manager

   /**
    * Constructeur par défaut.
    */
   public Serveur()
   {
      poolSocket = new ArrayList<Socket>();
      gm = new GameManager();
   }

   /**
    * Méthode exécutée tant que le serveur tourne. Attente de clients et ajouts de nouveaux au pool de clients.
    */
   public void run()
   {
      ServerSocket socketServeur = null;

      // On tente la création d'un socket de serveur
      try
      {
         socketServeur = new ServerSocket(4242);

         // On affiche sur quel port le serveur écoute
         System.out.println("Le serveur écoute sur le port " + socketServeur.getLocalPort() +
                                  " et est en version : 1");

         Journalisation.getLOG().fine("Le serveur à démarré et est prêt à traiter des clients");
      }
      catch (IOException e)
      {
         Journalisation.getERR().severe("Echec de la création des sockets SSL : " + e.getMessage());
      }

      // Tant que le serveur n'est pas tué, continuer de traiter les clients
      while (true)
      {
         try
         {
            // Attendre un client
            Socket socketClient = socketServeur.accept();

            // Ajouter le socket du client à la liste des clients
            poolSocket.add(socketClient);

            // Logger la connexion d'un client
            Journalisation.getLOG().fine("Client connecté depuis : " + socketClient.getInetAddress() + " : " + socketClient.getPort());

            // Un nouveau client est arrivé, lancer un thread pour gérer son socket
            Thread threadClient = new Thread(new GestionnaireSocket(this, socketClient));
            threadClient.start();
         }
         catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors de la création du thread du client : " + e.getMessage());
            return;
         }
      }
   }

   /**
    * Méthode permettant de supprimer un socket du pool de clients. Fonction appellée en général par un thread
    * client avant de se terminer. Permet de garder la liste des sockets ouverts vers les clients.
    *
    * @param socket Le socket à supprimer de la liste
    */
   public void supprimerSocket(Socket socket)
   {
      poolSocket.remove(socket);
   }

   /**
    * Méthode permettant la fermeture du serveur. Permet de fermer chaque socket de client les uns après les autres.
    */
   public void fermerServeur()
   {
      // Pour chacuns des sockets de la liste, les fermer
      for (Socket socket : poolSocket)
      {
         try
         {
            socket.close();
            ServicesSQL s = new ServicesSQL();
            s.stop();
         }
         catch (IOException e)
         {
            Journalisation.getERR().severe("Erreur lors de la fermeture des sockets clients : " + e.getMessage());
         }
      }
   }

   // GETTERS & SETTERS

   public GameManager getGm()
   {
      return gm;
   }
}
