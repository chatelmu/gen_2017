package Serveur.Serveur;

import Objets.Carte;
import Objets.Case;
import Protocole.Initialisations;

/**
 * Classe permettant de simplifier la construction d'une carte.
 */
public class ConstructeurCarte
{
   private Carte carte;

   /**
    * Constructeur élaboré
    *
    * @param carte La carte du Game Manager
    */
   public ConstructeurCarte(Carte carte)
   {
      this.carte = carte;
   }

   /**
    * Ajoute dans la carte concernée la case voulue à la bonne position.
    *
    * @param ligne        La ligne de la case
    * @param colonne      La colonne de la case
    * @param nouvelleCase La nouvelle case
    */
   public void configurerCase(int ligne, int colonne, Case nouvelleCase)
   {
      int indice = ((ligne - 1) * Initialisations.COTE_CARTE) + (colonne - 1);
      carte.getCases().set(indice, nouvelleCase);
   }
}
