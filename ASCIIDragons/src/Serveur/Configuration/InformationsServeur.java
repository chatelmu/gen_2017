package Serveur.Configuration;

/**
 * Classe contenant les informations rapport au serveur
 */
public class InformationsServeur
{
   public final static String VERSION = "1.0";

   public final static int DEFAULT_PORT = 4242;
   public final static String DEFAULT_HOTE = "localhost";

   public final static int NOMBRE_JOUEURS_MAX = 4;
}
