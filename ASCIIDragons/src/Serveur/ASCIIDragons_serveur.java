package Serveur;

import Serveur.Serveur.Serveur;
import Serveur.Utilitaires.Journalisation;

/**
 * Classe marquant le point d'entrée du serveur
 */
public class ASCIIDragons_serveur
{
   public static void main(String[] argv)
   {
      final Serveur serveur = new Serveur();
      Thread threadServer = new Thread(serveur);

      // On démarre le thread serveur
      threadServer.start();

		/*
      // Tests
      ReponseEtatFinDeTour reponseEtatFinDeTour = new ReponseEtatFinDeTour(new ReponseEtatJoueur(new Joueur("toto", new Socket(), 1, 1)), 10);
      String test = new Gson().toJson(reponseEtatFinDeTour);
      System.out.println(test);
      */

      // Intercepter la fin du programme (Ctrl + C), et termine toutes les connexion vers les clients afin d'être propre
      Runtime.getRuntime().addShutdownHook(new Thread()
      {
         public void run()
         {
            serveur.fermerServeur();
         }
      });

      // Attendre la fin du thread
      try
      {
         threadServer.join();
      }
      catch (InterruptedException e)
      {
         Journalisation.getLOG().severe("Problème durant l'attente du thread serveur : " + e.getMessage());
      }
   }
}
