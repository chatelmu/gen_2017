package Serveur.Hibernate;

import Objets.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Classe permettant de traiter les requêtes du client reçues par le GestionnaireClient.
 * Chaque méthode récupère la session vers la BDD et traite la requête.
 */
public class ServicesSQL
{
   private Session session;

   /**
    * Constructeur par défaut.
    */
   public ServicesSQL()
   {
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des monstres.
    *
    * @return Une liste des différents monstres présents dans la base de données
    */
   public List<Monstre> getListeMonstres()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Monstre> resultat = (List<Monstre>) session.createQuery("FROM Objets.Monstre").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
      finally
      {
         //HibernateUtil.getSessionFactory().close();
      }
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des décors.
    *
    * @return Une liste des différents décors présents dans la base de données
    */
   public List<Decor> getListeDecors()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Decor> resultat = (List<Decor>) session.createQuery("FROM Objets.Decor").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
      finally
      {
         //HibernateUtil.getSessionFactory().close();
      }
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des objets, ici on récupère les armures.
    *
    * @return Une liste des différentes armures présentes dans la base de données
    */
   public List<Armure> getListeArmures()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Armure> resultat = (List<Armure>) session.createQuery("FROM Objets.Armure").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des objets, ici on récupère les joyaux.
    *
    * @return Une liste des différents joyaux présents dans la base de données
    */
   public List<Joyau> getListeJoyaux()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Joyau> resultat = (List<Joyau>) session.createQuery("FROM Objets.Joyau").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des objets, ici on récupère les pommes.
    *
    * @return Une liste des différentes pommes présentes dans la base de données
    */
   public List<Pomme> getListePommes()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Pomme> resultat = (List<Pomme>) session.createQuery("FROM Objets.Pomme").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
   }

   /**
    * Méthode permettant de traiter la requête du client demandant la liste des objets, ici on récupère les armes.
    *
    * @return Une liste des différentes armes présentes dans la base de données
    */
   public List<Arme> getListeArmes()
   {
      Transaction tx = null;
      try
      {
         // Récupération de la session
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         tx = session.beginTransaction();

         // Traitement de la requête
         List<Arme> resultat = (List<Arme>) session.createQuery("FROM Objets.Arme").list();
         tx.commit();

         return resultat;
      }
      catch (HibernateException e)
      {
         if (tx != null)
         {
            tx.rollback();
         }
         e.printStackTrace();
         return null;
      }
   }

   /**
    * Méthode permettant de fermer la session d'Hibernate
    */
   public void stop()
   {
      HibernateUtil.getSessionFactory().close();
   }
}
