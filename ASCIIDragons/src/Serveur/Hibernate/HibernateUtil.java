package Serveur.Hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Classe représentant le lien vers la base de données avec Hibernate.
 */
public class HibernateUtil
{
   public static final SessionFactory sessionFactory; // La fabrique de session d'Hibernate

   static
   {
      try
      {
         // Création de la SessionFactory à partir de hibernate.cfg.xml
         sessionFactory = new Configuration().configure().buildSessionFactory();
      }
      catch (Throwable ex)
      {
         // Affiche l'exception en cas d'erreur
         System.err.println("Initial SessionFactory creation failed." + ex);
         throw new ExceptionInInitializerError(ex);
      }
   }

   public static final ThreadLocal session = new ThreadLocal(); // Le thread représentant la session

   /**
    * Méthode permettant de récupérer la fabrique de session.
    *
    * @return La sessionFactory d'Hibernate
    */
   public static SessionFactory getSessionFactory()
   {
      return sessionFactory;
   }
}
