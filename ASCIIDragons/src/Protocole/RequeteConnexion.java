package Protocole;

/**
 * Classe représentant une demande de connexion
 */
public class RequeteConnexion
{
   private String login;

   /**
    * Constructeur élaboré, utilisé par le client.
    *
    * @param login Le pseudonyme du joueur qui tente de se connecter
    */
   public RequeteConnexion(String login)
   {
      this.login = login;
   }

   // GETTERS & SETTERS

   public String getLogin()
   {
      return login;
   }

   public void setLogin(String login)
   {
      this.login = login;
   }
}
