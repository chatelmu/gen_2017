package Protocole;

import Objets.*;

/**
 * Classe représentant une réponse du changement de case.
 * Cet objet contient la nouvelle case mais aussi les booléens décrivants les directions accessibles.
 */
public class ReponsePossibiliteDeplacement
{
   private Case caseActuelle;    // La nouvelle case
   private boolean nord;         // Booléen indiquant si la case au nord est accessible ou non
   private boolean sud;          // Booléen indiquant si la case au sud est accessible ou non
   private boolean ouest;        // Booléen indiquant si la case à l'ouest est accessible ou non
   private boolean est;          // Booléen indiquant si la case à l'est est accessible ou non

   /**
    * Constructeur de ReponsePossibiliteDeplacement, utilisé par le serveur.
    *
    * @param caseActuelle La nouvelle case
    */
   public ReponsePossibiliteDeplacement(Case caseActuelle)
   {
      this.caseActuelle = caseActuelle;
   }

   // GETTERS & SETTERS

   public Case getCase()
   {
      return caseActuelle;
   }

   public boolean getNord()
   {
      return nord;
   }

   public void setNord(boolean nord)
   {
      this.nord = nord;
   }

   public boolean getSud()
   {
      return sud;
   }

   public void setSud(boolean sud)
   {
      this.sud = sud;
   }

   public boolean getOuest()
   {
      return ouest;
   }

   public void setOuest(boolean ouest)
   {
      this.ouest = ouest;
   }

   public boolean getEst()
   {
      return est;
   }

   public void setEst(boolean est)
   {
      this.est = est;
   }
}
