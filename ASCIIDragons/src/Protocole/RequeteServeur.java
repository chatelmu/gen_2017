package Protocole;

/**
 * Classe représentant les requetes possibles vers le serveur depuis le client. Utilisé pour toJSON et fromJSON
 */
public class RequeteServeur
{
   private int typeRequete;      // Le type de requête à traiter (selon Protocoles.ProtocoleASCIIDragons)
   private String[] arguments;   // Les éventuels arguments

   /**
    * Constructeur élaboré, utilisé par le client.
    *
    * @param typeRequete Le type de requête à envoyer (selon le protocole)
    * @param arguments   Les arguments pour la requête
    * @see ProtocoleASCIIDragons
    */
   public RequeteServeur(int typeRequete, String... arguments)
   {
      this.typeRequete = typeRequete;
      this.arguments = arguments;
   }

   /**
    * Constructeur par défaut
    */
   public RequeteServeur()
   {
   }

   // GETTERS & SETTERS

   public int getTypeRequete()
   {
      return typeRequete;
   }

   public String[] getArguments()
   {
      return arguments;
   }

   public void setTypeRequete(int typeRequete)
   {
      this.typeRequete = typeRequete;
   }

   public void setArguments(String[] arguments)
   {
      this.arguments = arguments;
   }
}
