package Protocole;

import Objets.Joueur;

/**
 * Classe qui représente l'état d'un joueur
 */
public class ReponseEtatJoueur
{
   private double pvActuels;  // Le nombre de PV actuels du joueur
   private double pvMax;      // Le nombre de points de vie maximum du joueur
   private int defenseAir;    // La résistance à l'air du joueur
   private int defenseTerre;  // La résistance à la terre du joueur
   private int defenseEau;    // La résistance à l'eau du joueur
   private int defenseFeu;    // La résistance au feu du joueur
   private int attaque;       // Le nombre de dégâts que le joueur inflige

   /**
    * Constructeur de l'état du joueur, utilisé par le serveur.
    *
    * @param joueur Le joueur dont on récupère les caractéristiques pour créer "l'état" de celui-ci
    */
   public ReponseEtatJoueur(Joueur joueur)
   {
      pvActuels = joueur.getPvActuels();
      pvMax = joueur.getPvMax();
      defenseAir = joueur.getDefenseAir();
      defenseTerre = joueur.getDefenseTerre();
      defenseEau = joueur.getDefenseEau();
      defenseFeu = joueur.getDefenseFeu();
      attaque = joueur.getAttaque();
   }

   // GETTERS & SETTERS

   public double getPvActuels()
   {
      return pvActuels;
   }

   public double getPvMax()
   {
      return pvMax;
   }

   public int getDefenseAir()
   {
      return defenseAir;
   }

   public int getDefenseTerre()
   {
      return defenseTerre;
   }

   public int getDefenseEau()
   {
      return defenseEau;
   }

   public int getDefenseFeu()
   {
      return defenseFeu;
   }

   public int getAttaque()
   {
      return attaque;
   }
}
