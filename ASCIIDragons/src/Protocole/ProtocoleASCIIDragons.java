package Protocole;

/**
 * Protocole utilisé lors des communications entre le serveur et le client
 */
public class ProtocoleASCIIDragons
{
   public final static int CMD_DEMANDER_MONSTRES = 1;
   public final static int CMD_DEMANDER_DECORS = 2;
   public final static int CMD_DEMANDER_OBJETS = 3;
   public final static int CMD_FERMER_SOCKET = 4;
   public final static int CMD_CONFIGURER_CASE = 5;
   public final static int CMD_DEBUT_PARTIE = 6;
   public final static int CMD_DEMANDE_DEPLACEMENT = 7;
   public static final int CMD_ATTAQUER = 8;
   public static final int CMD_FUIR = 9;
   public static final int CMD_RAMASSER_OBJET = 10;
   public static final int CMD_TOUR_JOUEUR = 11;
   public static final int CMD_PAS_TON_TOUR = 12;
   public static final int CMD_FIN_PARTIE = 13;
}
