package Protocole;

/**
 * Classe représentant l'état actuel en fin de tour
 */
public class ReponseEtatFinDeTour
{
   private ReponseEtatJoueur etatJoueur;  // Objet représentant "l'état" du joueur
   private int pvMonstre;                 // Les nouveaux pv du monstre (-1000 = inchangé)

   /**
    * Constructeur de reponseEtatFinDeTour, utilisé par le serveur.
    *
    * @param etatJoueur Le nouvel état du joueur
    * @param pvMonstre  Les nouveaux pv du monstre
    */
   public ReponseEtatFinDeTour(ReponseEtatJoueur etatJoueur, int pvMonstre)
   {
      this.etatJoueur = etatJoueur;
      this.pvMonstre = pvMonstre;
   }

   // GETTERS & SETTERS

   public ReponseEtatJoueur getEtatJoueur()
   {
      return etatJoueur;
   }

   public int getPvMonstre()
   {
      return pvMonstre;
   }
}
