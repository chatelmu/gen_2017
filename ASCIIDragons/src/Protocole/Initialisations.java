package Protocole;

/**
 * Classe contenant toutes les valeurs par défaut pour la configuration du serveur ou de la partie.
 */
public class Initialisations
{
   public final static int COTE_CARTE = 6;
   public final static int TAILLE_CARTE = COTE_CARTE * COTE_CARTE;

   public static final double PV_JOUEUR_INIT = 100;
   public static final double PV_JOUEUR_MAX = 100;
   public static final int DEFENSE_AIR_INIT = 0;
   public static final int DEFENSE_TERRE_INIT = 0;
   public static final int DEFENSE_EAU_INIT = 0;
   public static final int DEFENSE_FEU_INIT = 0;
   public static final int ATTAQUE_JOUEUR_INIT = 5;

   public static final int NOMBRE_DE_TOURS = 10;
}
