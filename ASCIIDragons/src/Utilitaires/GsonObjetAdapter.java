package Utilitaires;

import Objets.*;
import com.google.gson.*;

import java.util.Map;
import java.util.TreeMap;

/**
 * Classe représentant un adaptateur pour la sérialisation d'objets.
 */
public class GsonObjetAdapter implements JsonDeserializer<Objet>, JsonSerializer<Objet>
{
   private static Map<String, Class> map = new TreeMap<String, Class>(); // Map associant chaque classe spécialisée à une string

   static
   {
      map.put("Arme", Arme.class);
      map.put("Armure", Armure.class);
      map.put("Joyau", Joyau.class);
      map.put("Pomme", Pomme.class);
   }

   /**
    * Méthode appelée lors de la désérialisation d'un Objet.
    *
    * @param json    Le JSON à désérialiser
    * @param typeDeT Le type de l'objet à désérialiser (pas utilisé ici, on sait que c'est un Objet)
    * @param context Le contexte de désérialisation (pas utilisé ici)
    * @return L'objet Objet correspondant au JSON passé en paramètre
    * @throws JsonParseException Si la désérialisation a échoué
    */
   public Objet deserialize(JsonElement json, java.lang.reflect.Type typeDeT, JsonDeserializationContext context) throws JsonParseException
   {
      // On récupère le champs type de l'objet pour chercher la classe correspondante dans la map
      String type = json.getAsJsonObject().get("type").getAsString();
      Class classe = map.get(type);

      if (classe == null)
      {
         throw new RuntimeException("Classe inconnue : " + type);
      }

      // Quand on a déterminer la bonne classe on désérialise normalement
      Objet resultat = context.deserialize(json, classe);

      return resultat;
   }

   /**
    * Méthode permettant de sérialisé en JSON un Objet.
    *
    * @param src       L'Objet original à sérialiser
    * @param typeDeSrc Le type de la source (pas utilisé car on sait que c'est Objet)
    * @param context   Le contexte de sérialisation (pas utilisé ici)
    * @return L'élément JSONifié
    */
   public JsonElement serialize(Objet src, java.lang.reflect.Type typeDeSrc, JsonSerializationContext context)
   {
      if (src == null)
      {
         return null;
      }

      // On détermine la classe de l'objet à sérialiser
      Class classe = map.get(src.getType());

      if (classe == null)
      {
         throw new RuntimeException("Classe inconnue : " + classe);
      }

      // Une fois la classse correctement déterminée on sérialise normalement
      return context.serialize(src, classe);
   }
}
