package Utilitaires;

import Objets.*;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 * Classe représentant un adaptateur pour la sérialisation de ListeObjets.
 */
public class GsonListeObjetsAdapter implements JsonDeserializer<ListeObjets>, JsonSerializer<ListeObjets>
{
   private static Map<String, Class> map = new TreeMap<String, Class>(); // Map associant chaque classe spécialisée à une string

   static
   {
      map.put("Arme", Arme.class);
      map.put("Armure", Armure.class);
      map.put("Joyau", Joyau.class);
      map.put("Pomme", Pomme.class);
   }

   /**
    * Méthode appelée lors de la désérialisation d'une ListeObjet.
    *
    * @param json    Le JSON à désérialiser
    * @param typeDeT Le type de l'objet à désérialiser (pas utilisé ici, on sait que c'est une listeObjets)
    * @param context Le contexte de désérialisation (pas utilisé ici)
    * @return L'objet listeObjet correspondant au JSON passé en paramètre
    * @throws JsonParseException Si la désérialisation a échoué
    */
   public ListeObjets deserialize(JsonElement json, Type typeDeT, JsonDeserializationContext context) throws JsonParseException
   {
      ListeObjets reponse = new ListeObjets();
      JsonArray tableau = json.getAsJsonArray();

      for (JsonElement element : tableau)
      {
         // On récupère le champs type de l'objet actuel pour chercher la classe correspondante dans la map
         String type = element.getAsJsonObject().get("type").getAsString();
         Class classe = map.get(type);

         if (classe == null)
         {
            throw new RuntimeException("Classe inconnue : " + type);
         }

         // Quand on a déterminer la classe on désérialise normalement
         reponse.ajouterObjet((Objet) context.deserialize(element, classe));
      }

      return reponse;
   }

   /**
    * Méthode permettant de sérialisé en JSON une ListeObjet.
    *
    * @param src       La listeObjets original à sérialiser
    * @param typeDeSrc Le type de la source (pas utilisé car on sait que c'est ListeObjet)
    * @param context   Le contexte de sérialisation (pas utilisé ici)
    * @return L'élément JSONifié
    */
   public JsonElement serialize(ListeObjets src, Type typeDeSrc, JsonSerializationContext context)
   {
      if (src == null)
      {
         return null;
      }

      // L'objet qui sera sérialisé
      JsonArray tableau = new JsonArray();

      for (Objet o : src.getListe())
      {
         // On détermine la classe de l'objet actuel à sérialiser
         Class classe = map.get(o.getType());

         if (classe == null)
         {
            throw new RuntimeException("Classe inconnue : " + classe);
         }

         // Une fois la classe correctement déterminée on sérialise normalement
         tableau.add(context.serialize(o, classe));
      }

      return tableau;
   }
}
