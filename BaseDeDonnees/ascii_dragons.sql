-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 06 Avril 2017 à 13:27
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ascii_dragons`
--
CREATE DATABASE IF NOT EXISTS `ascii_dragons` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ascii_dragons`;

-- --------------------------------------------------------

--
-- Structure de la table `arme`
--

DROP TABLE IF EXISTS `arme`;
CREATE TABLE IF NOT EXISTS `arme` (
  `Id_Objet` int(11) NOT NULL,
  `Attaque` int(11) NOT NULL,
  KEY `Id_Objet` (`Id_Objet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `armure`
--

DROP TABLE IF EXISTS `armure`;
CREATE TABLE IF NOT EXISTS `armure` (
  `Id_Objet` int(11) NOT NULL,
  `DefenseEau` int(11) NOT NULL,
  `DefenseFeu` int(11) NOT NULL,
  `DefenseTerre` int(11) NOT NULL,
  `DefenseAir` int(11) NOT NULL,
  KEY `Id_Objet` (`Id_Objet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `decor`
--

DROP TABLE IF EXISTS `decor`;
CREATE TABLE IF NOT EXISTS `decor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Image` varchar(255) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `joyaux`
--

DROP TABLE IF EXISTS `joyaux`;
CREATE TABLE IF NOT EXISTS `joyaux` (
  `Id_Objet` int(11) NOT NULL,
  `Points` int(11) NOT NULL,
  KEY `Id_Objet` (`Id_Objet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `monstre`
--

DROP TABLE IF EXISTS `monstre`;
CREATE TABLE IF NOT EXISTS `monstre` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `PV` int(11) NOT NULL,
  `Force` int(11) NOT NULL,
  `Id_Type` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `objet`
--

DROP TABLE IF EXISTS `objet`;
CREATE TABLE IF NOT EXISTS `objet` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(25) NOT NULL,
  `Type` varchar(25) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pommes`
--

DROP TABLE IF EXISTS `pommes`;
CREATE TABLE IF NOT EXISTS `pomme` (
  `Id_Objet` int(11) NOT NULL,
  `Soins` int(11) NOT NULL,
  KEY `Id_Objet` (`Id_Objet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `arme`
--
ALTER TABLE `arme`
  ADD CONSTRAINT `arme_ibfk_1` FOREIGN KEY (`Id_Objet`) REFERENCES `objet` (`Id`);

--
-- Contraintes pour la table `armure`
--
ALTER TABLE `armure`
  ADD CONSTRAINT `armure_ibfk_1` FOREIGN KEY (`Id_Objet`) REFERENCES `objet` (`Id`);

--
-- Contraintes pour la table `joyaux`
--
ALTER TABLE `joyaux`
  ADD CONSTRAINT `joyaux_ibfk_1` FOREIGN KEY (`Id_Objet`) REFERENCES `objet` (`Id`);

--
-- Contraintes pour la table `pomme`
--
ALTER TABLE `pomme`
  ADD CONSTRAINT `pomme_ibfk_1` FOREIGN KEY (`Id_Objet`) REFERENCES `objet` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


SET @monstre1 =' oo``._..---.___..-\n (_,-.        ,..``\n      ``.    ;\n         : :`\n"         _;_; ';
SET @monstre2 = '        .-\"\"\"\".\n       /       \\\n   __ /   .-.  .\\\n  /  `\\  /   \\/  \\\n  |  _ \\/   .==.==.\n  | (   \\  /____\\__\\\n   \\ \\      (_()(_()\n    \\ \\            `---._\n     \\                   \\_\n  /\\ |`       (__)________/\n /  \\|     /\\___/\n|    \\     \\||VV\n|     \\     \\|\"\"\"\",\n|      \\     ______)\n\\       \\  /`\n         \\(';
SET @monstre3 = '      .-.\n     (o.o)\n      |=|\n     __|__\n   //.=|=.\\\\\n  // .=|=. \\\\\n  \\\\ .=|=. //\n   \\\\(_=_)//\n    (:| |:)\n     || ||\n     () ()\n     || ||\n     || ||\n    ==`` ``==';

INSERT INTO monstre (`Nom`, `Image`, `PV`, `Force`, `Id_Type`) VALUES ('Dinosaure',@monstre2,10,1,3);
INSERT INTO monstre (`Nom`, `Image`, `PV`, `Force`, `Id_Type`) VALUES ('Dracula',@monstre2,100,2,2);
INSERT INTO monstre (`Nom`, `Image`, `PV`, `Force`, `Id_Type`) VALUES ('Squelette',@monstre3,55,1,1);
SELECT * FROM monstre;


SET @decor1 ='      __   _\n    _(  )_( )_\n   (_   _    _)\n  / /(_) (__)\n / / / / / /\n/ / / / / /';
SET @decor2 ='           |\n       \\       /\n         .-\"-.\n    --  /     \\  --\n`~^~^~^-=======-~^~^~^~~^`\n`^~^_~-=========- -~^~^~^`\n`^_~^~~ -=====- ~^~^~-~^~`\n`~^~~-~^~^~-~^~~^-~^~^~^-`';
SET @decor3 ='          /\\\n         /**\\\n        /****\\   /\\\n       /      \\ /**\\\n      /  /\\    /    \\\n     /  /  \\  /      \\\n    /  /    \\/ /\\     \\\n   /  /      \\/  \\/\\   \\\n__/__/_______/___/__\\___\\\n_';

INSERT INTO decor (`Image`, `Nom`) VALUES (@decor1,"Nuage");
INSERT INTO decor (`Image`, `Nom`) VALUES (@decor2,"Plage");
INSERT INTO decor (`Image`, `Nom`) VALUES (@decor3,"Camping");


INSERT INTO objet (`Nom`,`Type`) VALUES ('Joyau 20 points','Joyau');
INSERT INTO joyaux (`Id_Objet`, `Points`) VALUES (LAST_INSERT_ID(),20);
INSERT INTO objet (`Nom`,`Type`) VALUES ('Pomme soigant 100 PV','Pomme');
INSERT INTO pomme (`Id_Objet`, `Soins`) VALUES (LAST_INSERT_ID(),100);
INSERT INTO objet (`Nom`,`Type`) VALUES ('Tertre des âmes','Arme');
INSERT INTO arme (`Id_Objet`, `Attaque`) VALUES (LAST_INSERT_ID(),3);
INSERT INTO objet (`Nom`,`Type`) VALUES ('Armure de flamme','Armure');
INSERT INTO armure (`Id_Objet`, `DefenseEau`,`DefenseFeu`,`DefenseTerre`,`DefenseAir`) VALUES (LAST_INSERT_ID(),0,10,0,0);
SELECT * FROM objet;